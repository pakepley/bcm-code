function mask = make_mask(o, tau)
%% --------------------------------------------------------------------
%% Function: make_mask
%% Use     : generate a logical mask. used to enforce support 
%%           constraint 
%% --------------------------------------------------------------------
%% Inputs:
%% o    - the bcm discretization structure
%% tau  - the support constraint function
%% --------------------------------------------------------------------
%% Outputs:
%% mask - the supporting function tau sampled on o.xs_src
%% --------------------------------------------------------------------

% Logical matrix, used to select entries
% mask = or( ...
%     abs(repmat(o.ts_src', 1, length(tau)) - (o.T - repmat(tau, o.nt_src,1))) ...
%     < o.epsilon, ...
%     repmat(o.ts_src', 1, length(tau)) > (o.T - repmat(tau, o.nt_src,1)));    

%mask =  repmat(o.ts_src', 1, length(tau)) > (o.T - repmat(tau, o.nt_src,1)) ...
        ;
mask =  repmat(o.ts_src', 1, length(tau)) - (o.T - repmat(tau, o.nt_src,1)) ...
        > .00001;


end

