import os, sys
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

## get the data path
if '-B' in sys.argv:
	sys.argv.pop(sys.argv.index('-B'))
data_path = sys.argv[1]
sys.path.append(data_path)

## get the ray
from wave_speed        import *
from ray_trace_params  import *
from ray_trace         import *

## xs_rec belongs to femparams
ps_src = np.array([[x, 0.0] for x in xs_src])
nx_src = len(xs_src)
mx_src = int(np.ceil(nx_src / 2))

## the control time belongs to femparams
nthetas = 100
nsteps  = 100

## if we have included a time T, ignore the time T
## included from ray_trace_params
if len(sys.argv) > 2:
	T = float(sys.argv[2])

i = mx_src
print 'Computing rays for xs_src[{0}]={1}'.format(i,xs_src[i])
xs = fire_ray_fan(np.array(ps_src[i]), 
				  nthetas, nsteps, T)

print np.shape(xs)
fig = plt.figure()
for i in range(nthetas):
	plt.plot(xs[:,0,i], xs[:,1,i], color='b')
plt.show()
