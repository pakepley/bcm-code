import os, sys
import numpy as np
import scipy.io as sio
import scipy.integrate as sint
#import matplotlib.pyplot as plt

def dd(p_a,p_b):
	xa = p_a[0]
	xb = p_b[0]
	ya = p_a[1]
	yb = p_b[1]
	return np.arccosh(1.0 + ((xa - xb)**2  + (ya - yb)**2) /  
					  (2 * (1.0 + ya) * (1.0 + yb)))

## get the data path
if '-B' in sys.argv:
	sys.argv.pop(sys.argv.index('-B'))
data_path = sys.argv[1]
sys.path.append(data_path)

## get the ray
from wave_speed        import *
from ray_trace_params  import *
from ray_trace         import *

## xs_rec belongs to femparams
ps_src = np.array([[x, 0.0] for x in xs_src])
nx_src = len(xs_src)
mx_src = int(np.ceil(nx_src / 2))

## for now, read from file
s = float(sys.argv[2])   # travel time inward
y = float(sys.argv[3])   # starting point y-coord
z = float(sys.argv[4])   # z y-coord

p_y = np.array([y,0.0])
p_z = np.array([z,0.0])

## compute the point x(y,s) and the covector which aims from
## x(y,s) back at the point py
nu_y   = np.array([0.0,1.0])
nu_y   = g_normalize_covec(p_y,nu_y)
step_size = 1.0 / 10000.0
nsteps    = int(ceil(s / step_size))
x_y_s, minus_xi_xys_to_y  = solve_hamilton_jacobi(p_y,nu_y,nsteps,s,2)
xi_xys_to_y = -1.0 * minus_xi_xys_to_y

## compute the angle of the covector which aims from x(y,s)
## back at py, we use this to form a ray fan emanating from x(y,s)
## which includes a ray that connects x(y,s) to py
theta_xi_xys_to_y = np.arctan2(xi_xys_to_y[1], xi_xys_to_y[0])


## now we will estimate an upper bound for d(x(py,s), pz)
## see that: 
##
##       d(x(py,s),pz) \leq d(x(py,s), py) + d(py,pz)
##                     \leq s              + length(ll)
##
## where ll is any path connecting py to pz
## in particular, we choose ll(t) = py + t * (pz-py)
## Then, because:
##
##    length(ll) = |\int_y^z  \| d(ll(t))/dt \|_g dt|
##
## and  
##    \| d(ll(t))/dt \|_g =  \|pz - py\|_e / c(ll(t))
##
## this implies we can compute
##
##    length(ll) = \|pz - py\|_e  | \int_y^z  (1/c(ll(t)) dt |
##
## which we compute below

# define the line connecting py to pz
def ll(t):
	return p_y + t * (p_z - p_y)

# next define the integrand 1/c on the path ll
def integrand(t):
	return 1.0 / c(ll(t))

# compute the upper bound on d(py,pz)
d_py_pz_ub = np.linalg.norm(p_y - p_z) * abs(sint.quad(integrand, y, z)[0])

# compute the upper bound on d(x(p_y,s), p_z)
# use the expression above plus multiplied by a fudge factor ff
ff = 2.0
d_xpys_pz_ub = (s + d_py_pz_ub) * ff

## now we will fire rays around [0, 2*pi]. one of these has been selected
## to aim at the point y, and some of these 
nthetas   = 10
step_size = 1.0 / 10000.0
nsteps    = int(ceil(d_xpys_pz_ub / step_size))
print nsteps
if   y >= z:
	theta_lo = theta_xi_xys_to_y - 2.0 * np.pi
	theta_hi = theta_xi_xys_to_y
elif y <  z:
	theta_lo = theta_xi_xys_to_y 
	theta_hi = theta_xi_xys_to_y + 2.0 * np.pi


#plt.ion()
#fig = plt.figure()

## given a guess as to what angle interval will aim at z, 
def get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps):
	print 'We will be plotting in the interval theta in [{0},{1}]'.format(theta_lo, theta_hi)
	# compute a rays fan corresponding to rays with initial covectors at angles
	# between theta_lo and theta_hi
	pp = fire_ray_fan(x_y_s, nthetas, nsteps, d_xpys_pz_ub, theta_lo, theta_hi)
	thetas = np.linspace(theta_lo, theta_hi, nthetas + 1)

	# the final x and y values:
	xs        = pp[-1, 0, :]
	ys        = pp[-1, 1, :]                

	# find the indices where the ray enters the boundary and is either
	# less than z or greater than z:
	inds_lt_z = []
	inds_gt_z = []
	for i in range(0,nthetas + 1):
		if abs(ys[i]) < 10**-5 or ys[i] <= 0.0:
			if xs[i] <= z:
				inds_lt_z.append(i)
			elif xs[i] >= z:
				inds_gt_z.append(i)

	print 'lt z', thetas[inds_lt_z]
	print 'gt z', thetas[inds_gt_z]

	# the x coordinates of the final ray posistions which end up either 
	# less than z or greater than z:
	xs_lt_z = list(xs[inds_lt_z])
	xs_gt_z = list(xs[inds_gt_z])

	# find which indices correspond the the max x less than z
	# and the min x greater than z:
	max_xs_lt_z   = max(xs_lt_z)
	imltz         = xs_lt_z.index(max_xs_lt_z)
	ind_max_lt_z  = inds_lt_z[imltz]
	min_xs_gt_z   = min(xs_gt_z)
	imgtz         = xs_gt_z.index(min_xs_gt_z)
	ind_min_gt_z  = inds_gt_z[imgtz]

	all_inds = list(set(inds_lt_z) | set(inds_gt_z))

	#plt.clf()
	#plt.plot(pp[:,0,all_inds], pp[:,1,all_inds])
	#plt.draw()
	
	print ind_max_lt_z, thetas[ind_max_lt_z], xs[ind_max_lt_z]
	print ind_min_gt_z, thetas[ind_min_gt_z], xs[ind_min_gt_z]
	
	theta_lo = min([thetas[ind_max_lt_z], thetas[ind_min_gt_z]])
	theta_hi = max([thetas[ind_max_lt_z], thetas[ind_min_gt_z]])

	return theta_lo, theta_hi

nthetas   = 10
step_size = 1.0 / 1000.0
nsteps    = int(ceil(d_xpys_pz_ub / step_size))
theta_lo, theta_hi = get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps)
theta_lo, theta_hi = get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps)
theta_lo, theta_hi = get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps)
theta_lo, theta_hi = get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps)
theta_lo, theta_hi = get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps)
theta_lo, theta_hi = get_angles_aiming_near_z(theta_lo, theta_hi, nthetas, nsteps)

theta_mid = 0.5 * (theta_lo + theta_hi)
xi_from_xys_to_z = np.array([np.cos(theta_mid),np.sin(theta_mid)])
xi_from_xys_to_z = g_normalize_covec(x_y_s, xi_from_xys_to_z)
dd_approx, z_approx = solve_hamilton_jacobi(x_y_s, xi_from_xys_to_z, nsteps, d_xpys_pz_ub, 6)

print z, z_approx
print dd(p_z, x_y_s), dd_approx
