import os, sys
from mpi4py import MPI
import numpy as np
import scipy.io as sio

## get the data path
if '-B' in sys.argv:
	sys.argv.pop(sys.argv.index('-B'))
data_path = sys.argv[1]
sys.path.append(data_path)

## get the ray
from wave_speed        import *
from ray_trace_params  import *
from ray_trace         import *

## set up the communicator
comm = MPI.COMM_WORLD
comm_rank = comm.Get_rank()
comm_size = comm.Get_size()

## xs_rec belongs to femparams
ps_src = np.array([[x, 0.0] for x in xs_src])
nx_src = len(xs_src)

## the control time belongs to femparams
nthetas = 10
nsteps  = 100

## initialize lists to hold the local lo values
xlo = []; xhi  = []; ylo = []; yhi = [];

for i in range(comm_rank, nx_src, comm_size):	
	print 'Computing rays for xs_src[{0}]={1}'.format(i,xs_src[i])
	xl,xh,yl,yh = getranges(fire_ray_fan(np.array(ps_src[i]), 
										 nthetas, nsteps, T))
	# save the data in a list
	xlo.append(xl); xhi.append(xh);	ylo.append(yl);	yhi.append(yh);

## compute the local extrema and block before computing global extrema
local_los = np.array((np.min(np.array(xlo)),np.min(np.array(ylo))))
local_his = np.array((np.max(np.array(xhi)),np.max(np.array(yhi))))
comm.Barrier()

## store space for the global extrema
if comm_rank==0:
    # only processor 0 will actually get the data
    global_los = np.zeros_like(local_los)
    global_his = np.zeros_like(local_his)
else:
    global_los = None
    global_his = None


## compute the global maxima
comm.Reduce(
    [local_his,  MPI.DOUBLE],
    [global_his, MPI.DOUBLE],
    op = MPI.MAX,
    root = 0
)

# compute the global minima
comm.Reduce(
    [local_los,  MPI.DOUBLE],
    [global_los, MPI.DOUBLE],
    op = MPI.MIN,
    root = 0
)
comm.Barrier()

if comm_rank == 0:
	print '[{0}]: [xlo,ylo]={1}'.format(comm_rank, global_los)
	print '[{0}]: [xhi,yhi]={1}'.format(comm_rank, global_his)

	## extract the domain extrema and save to file
	domain_extrema_mat_file = '{0}/domain_extrema.mat'.format(data_path)
	xlo = global_los[0]
	xhi = global_his[0]
	yhi = global_his[1]	
	print 'Saving in file {0}'.format(domain_extrema_mat_file)
	sio.savemat(domain_extrema_mat_file, {'X0': xlo,
										  'X1': xhi,
										  'Y' : yhi})
