%% flags to tell us what gets run and when
init_discr_flag      =  false;
run_fem_solver_flag  =  false;
compute_matrix_flag  =  false;
run_wvfld_recov_flag =  false;

%% flags for the interior control problem
init_interior_discr_flag              = false;
run_interior_fem_solver_flag          = false;
compute_halftime_matrix_flag          = false;
move_receivers_flag                   = false;
run_int_src_wvfld_recov_flag          = false;
run_int_src_wvfld_recov_mult_ts_flag  =  true;

%% set up some flags, Octave doesn't like the || operator, so we use or()
need_matrices_flag   = compute_matrix_flag || run_wvfld_recov_flag || ...
    run_int_src_wvfld_recov_flag || run_int_src_wvfld_recov_mult_ts_flag ...
    || move_receivers_flag;

need_halftime_matrices_flag = compute_halftime_matrix_flag || ...
    run_int_src_wvfld_recov_flag || run_int_src_wvfld_recov_mult_ts_flag;

need_int_save_path_flag   = run_interior_fem_solver_flag || ...
    need_halftime_matrices_flag || run_int_src_wvfld_recov_mult_ts_flag;


%% Include some helper functions and raytracers
src_dir = pwd();
addpath([src_dir '/io_facilities']);
addpath([src_dir '/ray_trace']);    

%% Get some directory information
user_home = getenv('HOME');

%% the parent directory for all save information
%% default is to point to $HOME/Computational_Data
save_base = [user_home '/Computational_Data'];
conditional_mkdir(save_base);

save_root = [save_base '/redatuming_gen_src'];
conditional_mkdir(save_root)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%            Initialize the discretization of the problem             %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if init_discr_flag
    %% prep for storage
    save_path = [save_root '/' timestamp()];
    conditional_mkdir(save_path);    
    
    %% take a snap shot of the source code
    save_source_code(save_path);
    
    %% Set up the parameters
    domain_type        = 'half_space' ;
    data_type          =         'ND' ;
    start_type         =       'early';
    T                  =         2.00 ;
    t_separation       =         0.05 ;
    L                  =         6.00 ;
    spatial_separation =         0.05 ;
    n_processors       =            3 ;
    c_code             =        '1.0' ;
    transl_invar_flag  =         true ;
    y_in               =         1.00 ;
        
    addpath([src_dir '/' domain_type]);    
    if strcmp(domain_type, 'half_space')
        discr = init_params(save_path, data_type, start_type, T, ...
                            t_separation, L, spatial_separation, c_code, ...
                            n_processors, transl_invar_flag, y_in);
    else
        error(['This code only runs the half-space case. Something went ' ...
               'wrong, since the discretization is being set up for ' ...
               'non half-space!']);
    end

    %% save the discretization for later
    save([save_path '/discr.mat'], 'discr', '-v7');

elseif not(exist('save_path', 'var'))
    % prompt the user for a save_path
    save_path = input('Save_Path not set! Where is the data located?\n', 's');
    load([save_path '/discr.mat']);
end

addpath(['./' discr.domain_type]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%              Run the forward finite element solver                  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_fem_solver_flag    
    % Save the source files so that we can reproduce the simulations
    % zip([save_path '/src.zip'], {'*.m', '*.py'});

    % Save the command window output
    diary([save_path '/diary_fem.txt']);
    
    % run the solver in forward mode
    run_femsolver(discr, save_path);

    % export the diary 
    diary off;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   Compute matrices used in approximately solving control problems   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if need_matrices_flag
    if not(isfield('discr','coeffs_K') || isfield('discr','coeffs_Ginv') ...
           || isfield('discr','coeffs_G'))
        discr = get_matrices(discr,save_path);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    Solve control problem Kh = Kf, where f is a long-time source     %%
%%    function and h has a much smaller temporal support               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_wvfld_recov_flag
    output_root = [save_path '/wavefield_recovery'];
    conditional_mkdir(output_root);

    output_root = [output_root '/' timestamp()];       
    conditional_mkdir(output_root);

    
    for i = 0 : (discr.nt_src + 1)
         
        output_path = [output_root sprintf('/time_%f', discr.T - i*discr.t_separation) ];    
        discr.output_path = output_path;
        conditional_mkdir(output_path);

        t_c     = 0.25 + discr.t_separation * i;
        p_c     = 0.0;
        sigma_h = 0.1;
        [rhs_integral, f_coeffs] = generate_test_data(discr, t_c, p_c, sigma_h);
        
        % Control set \Gamma, we currently use full boundary
        R    =                     .75   ; 
        tau  =      make_tau(discr, R)  ;
        mask =    make_mask(discr, tau) ;
        chi  =            double(mask)  ;

        % Control Prob solver params
        Nouter   =   300;    % maximum n outer GMRES iters
        Nrestart =    20;    % maximum n inner GMRES iters
        tol      =  1e-6;    % tolerance for GMRES 
        alpha    =  5e-5;    % Tikhonov regularization parameter
        discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, tol);

        %% Coefficients of the control problem's solution
        h_coeffs = solve_bc(discr, discr_inversion, rhs_integral, tau);

        % Save the coefficients
        f_coeff_path = save_coeffs(discr, save_path, f_coeffs, output_path, 'coeffs_f');
        h_coeff_path = save_coeffs(discr, save_path, h_coeffs, output_path, 'coeffs_h');
        diff_coeff_path = save_coeffs(discr, save_path, f_coeffs - h_coeffs, ...
                                      output_path, 'coeffs_diff');

        % % Compute the difference wave-field    
        % run_femsolver(discr, save_path, 'coeff_mode', f_coeff_path);
        % run_femsolver(discr, save_path, 'coeff_mode', h_coeff_path);
        % run_femsolver(discr, save_path, 'coeff_mode', diff_coeff_path);
    end
    
    % % write pvd files to show time stepping
    % system(['python ./io_facilities/write_time_series_pvd.py ' output_root]);
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%      Initialize the discretization of the interior problem          %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if init_interior_discr_flag
    %% the spatial separation is named differently in both cases
    if discr.domain_type == 'half_space'
        spatial_separation = discr.x_separation;
    elseif discr.domain_type == 'disk'
        spatial_separation = discr.theta_separation;
    end
    
    int_save_root = [save_path '/interior_waves'];
    conditional_mkdir(int_save_root);
    
    int_save_path = [int_save_root '/' timestamp()];
    conditional_mkdir(int_save_path);
        
    discr_int = init_params(int_save_path, ...
                            'ND_INT', ...
                            discr.start_type, ...
                            discr.T / 2, ... 
                            discr.t_separation, ...
                            discr.L_src, ... 
                            spatial_separation, ...
                            discr.c_code, ...
                            discr.n_processors, ...
                            discr.transl_invar_flag, ...
                            discr.Y);
        
    %% save the discretization for later
    save([int_save_path '/discr_int.mat'], 'discr_int', '-v7'); % save
                                                                % workspace
                                                                % to be
                                                                % re-read
    
    % change the data type to interior source problem
    xc =           0.00 ;
    yc =           0.25 ;
    tc = discr.ts_src(1);
    save_interior_femparams(discr_int, int_save_path, xc, yc, tc);    
    
elseif and( not(exist('int_save_path', 'var')), need_int_save_path_flag)
    % prompt the user for a save_path
    int_save_root = [save_path '/interior_waves'];
    int_save_path = input('int_save_path not set! Where is the data located?\n', 's');
    load([int_save_path '/discr_int.mat']);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%           Run the forward interior finite element solver            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_interior_fem_solver_flag
    %% take a snap shot of the source code
    save_source_code(int_save_path);
    
    % Save the command window int_save_path
    diary([int_save_path '/diary_fem.txt']);

    % run the solver in interior data mode
    run_femsolver(discr_int, int_save_path);

    % export the diary 
    diary off;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute matrices used in approx. solving halftime control problems  %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if need_halftime_matrices_flag
    if or( not(isfield('discr_int','coeffs_K')), ... 
           not(isfield('discr_int','coeffs_invG')) )
        int_matrix_path = [int_save_root '/matrices'];
        discr_int = get_matrices(discr_int, save_path, int_matrix_path);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                 Compute the moving receivers map, L                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if move_receivers_flag
    
    %% how deep int M do we know M?
    R = .5;

    %% make directories to hold data 
    L_save_root = [save_path '/moved_receivers'];
    conditional_mkdir(L_save_root);
    L_save_path = [L_save_root sprintf('/L_R_%.04f', R)];
    conditional_mkdir(L_save_path);
        

    %% we need sol probe measurements to build L
    if not(exist([L_save_path '/sol'], 'dir'))

        %% the spatial separation is named differently in both cases
        if discr.domain_type == 'half_space'
            spatial_separation = discr.x_separation;
        elseif discr.domain_type == 'disk'
            spatial_separation = discr.theta_separation;
        end
        
        %% discretization for FEM solver, etc
        discr_R = init_params(L_save_path, ...
                              'ND', ...
                              discr.start_type, ...
                              R, ... 
                              discr.t_separation, ...
                              discr.L_src, ... 
                              spatial_separation, ...
                              discr.c_code, ...
                              discr.n_processors, ...
                              discr.transl_invar_flag, ...
                              discr.Y);
        %% save the discretization for later
        save([L_save_path '/discr_R.mat'], 'discr_R', '-v7');
    else
        load([L_save_path '/discr_R.mat']);
    end
    

    %% we need sol probe measurements to build L
    if not(exist([L_save_path '/sol'], 'dir'))
        % run the solver in forward mode
        run_femsolver(discr_R, L_save_path, 'sol_probe_mode', L_save_path);
    end
    
    if not(exist([L_save_path '/sampling_matrix.mat'], 'file'))
        sampling_matrix = build_sampling_matrix(discr_R, ...
                                                [L_save_path '/sol'], ...
                                                discr_R.xs_sol, ...
                                                discr_R.ys_sol);
        save([L_save_path '/sampling_matrix.mat'], 'sampling_matrix', '-v7');
    else
        load([L_save_path '/sampling_matrix.mat']);
    end

    L_work_path = [L_save_path '/L_i'];
    conditional_mkdir(L_work_path);

    for i = 1:discr_R.nx_src
        L_i_1_path = sprintf([L_work_path '/L_%d_1.mat'], i);
        if not(exist(L_i_1_path, 'file'))            
            L_i_1 = compute_L(discr, discr_R, i, R, sampling_matrix);
            save(L_i_1_path, 'L_i_1', '-v7');
        end
    end

    
end



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%    Solve control problem Kh = Kf, where f is a long-time source     %%
% %%    function and h has a much smaller temporal support               %%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if run_int_src_wvfld_recov_flag

%     % we need sol probe measurements to run the interior wvfld recovery
%     if not(exist([save_path '/sol'], 'dir'))
%         % run the solver in forward mode
%         run_femsolver(discr, save_path, 'sol_probe_mode', save_path);
%     end
    
%     % currently I can only handle a constant wave-speed
%     if not(discr.transl_invar_flag)
%         error('Not set up to handle non-layered media yet');
%     end
    
%     % Create directories to hold data
%     output_root = [int_save_path '/wavefield_recovery'];
%     conditional_mkdir(output_root);
%     output_path = [output_root '/' timestamp()];
%     conditional_mkdir(output_path);

%     % Take a snapshot of the source code
%     save_source_code(output_path);
        
%     %% Control set \Gamma, we currently use full boundary
%     R    =                        0.5 ; 
%     tau  =     make_tau(discr_int, R) ;
%     mask =  make_mask(discr_int, tau) ;
%     chi  =               double(mask) ;
        
%     %% compute the rhs for the control problem
%     load([int_save_path '/F_params.mat']);
    
%     [proj_along_RF, proj_along_JF] = proj_along_int_src_windowed(discr, ...
%                                                       discr_int, save_path, ...
%                                                       F_params);
    
%     rhs_integral = compute_int_src_rhs(discr, discr_int, proj_along_RF, ...
%                                        proj_along_JF);

    
%     % Control Prob solver params
%     Nouter   =   300;    % maximum n outer GMRES iters
%     Nrestart =    20;    % maximum n inner GMRES iters
%     tol      =  1e-6;    % tolerance for GMRES 
%     alpha    =  5e-5;    % Tikhonov regularization parameter
%     discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, tol);

%     % Coefficients of the control problem's solution
%     k_coeffs = solve_bc(discr_int, discr_inversion, rhs_integral, tau);    
%     tmp = zeros(discr.nt_src, discr.nx_src);
%     tmp(1: discr_int.nt_src, :) = ...
%         reshape(k_coeffs, discr_int.nt_src, discr_int.nx_src);
%     k_coeffs = tmp;

%     %% Save things to file.
%     save([output_path '/data.mat'], 'k_coeffs', 'proj_along_RF', ...
%          'proj_along_JF', 'tau', 'R', '-v7');
%     save([output_path '/discr_inversion.mat'], 'discr_inversion','-v7');

%     %% Save the coefficients to pass to the FEM solver
%     k_coeff_path = save_coeffs(discr, save_path, k_coeffs, ...
%                                output_path, 'k_coeffs');

%     %% Plot the solution
%     disp('Computing the solution wave-field uk_T_2 for plotting');
%     run_femsolver(discr, save_path, 'coeff_mode', k_coeff_path);    
        
% end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    Solve control problem Kh = Kf, where f is a long-time source     %%
%%    function and h has a much smaller temporal support               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_int_src_wvfld_recov_mult_ts_flag

    %% we need sol probe measurements to run the interior wvfld recovery
    if not(exist([save_path '/sol'], 'dir'))
        % run the solver in forward mode
        run_femsolver(discr, save_path, 'sol_probe_mode', save_path);
    end
    
    %% Currently I can only handle a constant wave-speed
    if not(discr.transl_invar_flag)
        error('Not set up to handle non-layered media yet');
    end

    %% Create directories to hold data
    output_root = [int_save_path '/wavefield_recovery'];
    conditional_mkdir(output_root);

    output_sub_root = [output_root '/' timestamp()];
    conditional_mkdir(output_sub_root);

    %% Take a snapshot of the source code
    save_source_code(output_sub_root);

    %% Control set \Gamma, we currently use full boundary
    R    =                        0.5 ; 
    tau  =     make_tau(discr_int, R) ;
    mask =  make_mask(discr_int, tau) ;
    chi  =               double(mask) ;
        
    %% These need to exist for the wavefield recovery
    L_save_root = [save_path '/moved_receivers'];
    L_save_path = [L_save_root sprintf('/L_R_%.04f', R)];
    load([L_save_path '/discr_R.mat']);    
    
    %% compute the rhs for the control problem
    load([int_save_path '/F_params.mat']);
    
    for k = 0:discr_int.nt_src+1
        %% how much we will delay F
        delay_time    = discr_int.t_separation      * k;        
        delay_padding = discr.ratio_t_separation_dt * k;

        %% when we will compute w^F
        snapshot_time = discr.T / 2.0 - delay_time
        
        %% make a directory to hold output
        output_path   = [output_sub_root sprintf('/w_F_t_%.04f', snapshot_time)];
        conditional_mkdir(output_path);


        %% parameters for the delayed version of F, F_delayed
        F_delayed_params    = F_params;
        F_delayed_params.tc = F_delayed_params.tc + delay_time;
    
        [proj_along_RF, proj_along_JF] = proj_along_int_src_windowed(discr, ...
                                                          discr_int, ...
                                                          discr_R, save_path, ...
                                                          F_delayed_params);
        
        rhs_integral = compute_int_src_rhs(discr, discr_int, proj_along_RF, ...
                                           proj_along_JF);
        
    
        % Control Prob solver params
        Nouter   =   300;    % maximum n outer GMRES iters
        Nrestart =    20;    % maximum n inner GMRES iters
        tol      =  1e-6;    % tolerance for GMRES 
        alpha    =  5e-4;    % Tikhonov regularization parameter
        discr_inversion = set_inversion_params(Nouter, Nrestart, alpha, tol);

        % Compute coefficients of the control problem's solution
        k_coeffs = solve_bc(discr_int, discr_inversion, rhs_integral, tau);    
        k_coeffs = reshape(k_coeffs, discr_int.nt_src, discr_int.nx_src);
        
        %% prepare coefficients for plotting
        k_coeffs_plot = k_coeffs(end - discr_R.nt_src + 1 : end, :);

        %% Save things to file.
        save([output_path '/data.mat'], 'k_coeffs', 'k_coeffs_plot', ...
             'proj_along_RF', 'proj_along_JF', 'tau', 'R', '-v7');
        save([output_path '/discr_inversion.mat'], 'discr_inversion', '-v7');

        %% Save the coefficients to pass to the FEM solver
        k_coeff_path = save_coeffs(discr_R, L_save_path, k_coeffs_plot, ...
                                   output_path, 'k_coeffs');

        %% Plot the solution
        disp('Computing the solution wave-field uk_T_2 for plotting');
        run_femsolver(discr_R, L_save_path, 'coeff_mode', k_coeff_path);    
    end
end
