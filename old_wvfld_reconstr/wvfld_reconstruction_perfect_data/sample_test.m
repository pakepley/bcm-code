function [f,uu,psi,S] = sample_test(o, save_path, i0, j0, li, lj, alpha)
    
    %S = build_sample_matrix(o, save_path, i0, j0, li, lj);
    [f, psi, S] = solve_sample_problem(o, save_path, i0, j0, li, lj, alpha);
    uu = plot_sample_solution(o, save_path, i0, j0, li, lj, f, psi, S);
    
end