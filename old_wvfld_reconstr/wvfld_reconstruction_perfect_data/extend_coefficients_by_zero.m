function f_global = extend_coefficients_by_zero(f_local, o, i0, j0, li, lj)
    f_local = reshape(f_local, 2*lj+1, 2*li+1);
    f_global = zeros(o.nt_src, o.nx_src);
    f_global(j0 - lj : j0 + lj, i0 - li : i0 + li) = f_local;
end 