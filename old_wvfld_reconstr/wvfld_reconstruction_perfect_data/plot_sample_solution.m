function uu = plot_sample_solution(o, save_path, i0, j0, li, lj, f, psi, S)

    sol_path = [save_path '/sol'];

    nt_src = o.nt_src;
    nx_src = o.nx_src;    
    xs_src = o.xs_src;

    n_stencil_i = 2*li + 1;
    n_stencil_j = 2*lj + 1;
    n_stencil_sq = n_stencil_i * n_stencil_j;

    uu = zeros(nx_src, nt_src);
    
    % how many more indices on the left in xs_sol than xs_src?
    left_pad = (o.nx_sol - o.nx_src) / 2;
    xinds = left_pad + 1 : left_pad + o.nx_src;
    yinds = 1 : o.nt_src;

    for p = -li:li
        i  = i0 + p;  
        pp = li + p + 1;
        uf_i_1 = get_sol(o, sol_path, i);        
        for q = -lj:lj
            %j  = o.nt_src + 1 - (j0 + q); 
            j  = (j0 + q); 
            qq = lj + q + 1;
            uf_i_j = get_windowed_uf_ij(o, uf_i_1, i, j, xinds, yinds);
            SS_i_j = squeeze(uf_i_j(o.mt_rec, :, :));
            uu = uu + f( n_stencil_j*(pp-1) + qq ) * SS_i_j;            
        end
    end

    figure(3);
    subplot(4,1,1);
    imagesc(uu);
    colorbar
    subplot(4,1,2);
    imagesc(reshape(S*f,n_stencil_i, n_stencil_j));
    colorbar
    subplot(4,1,3);
    psi = reshape(psi, n_stencil_i, n_stencil_j);
    imagesc(psi);
    colorbar
    % subplot(4,1,4);
    % imagesc(uu( j0-lj : j0+lj, i0-li : i0 +li) - psi);
    % colorbar
    
    
end