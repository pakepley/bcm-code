function [f, psi, S] = solve_sample_problem(o, save_path, i0, j0, li, lj, alpha)

    n_stencil_i = 2*li + 1;
    n_stencil_j = 2*lj + 1;
    n_stencil_sq = n_stencil_i * n_stencil_j;
    
    nt_src = o.nt_src;
    nx_src = o.nx_src;    
    xs_src = o.xs_src;
    
    % build the sampling matrix
    S = build_sample_matrix(o, save_path, i0, j0, li, lj);
    
    figure(6)
    imagesc(S);
    N_alpha = (S' * S + alpha * eye(n_stencil_sq, n_stencil_sq));
    
    cond(S' * S)
    cond(N_alpha)
    figure(7)
    imagesc(N_alpha)
    
    % compute the target samples
    psi = zeros(n_stencil_sq,1);
    psi = (xs_src(i0-li : i0 + li)' - xs_src(i0)).^2;
    psi = repmat(psi, 1, n_stencil_j);
    psi = psi(:);
    
    % solve the data fitting problem (control problem)
    f = gmres(N_alpha, (S' * psi), 20, [], 100); 
    
    figure(1);
    imagesc(reshape(f,n_stencil_j, n_stencil_i));
    
    figure(2)
    subplot(2,1,1);
    imagesc(reshape(psi,n_stencil_j, n_stencil_i));

    subplot(2,1,2);
    imagesc(reshape(S * f, n_stencil_j, n_stencil_i));
end