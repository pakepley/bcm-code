function S = build_sample_matrix(o, save_path, i0, j0, li, lj)

    sol_path = [save_path '/sol/'];
    
    n_stencil_i = 2*li + 1;
    n_stencil_j = 2*lj + 1;
    n_stencil_sq = n_stencil_i * n_stencil_j;
    nx_src = o.nx_src;
    nt_src = o.nt_src;
    
    S = zeros(n_stencil_sq, n_stencil_sq);

    % how many more indices on the left in xs_sol than xs_src?
    left_pad = (o.nx_sol - o.nx_src) / 2;
    xinds = left_pad + (i0 - li) : left_pad + (i0 + li)
    yinds = j0 - lj : j0 + lj
    
    for p = -li:li
        i = i0 + p;
        
        %% get the samples of u^{f_i1}(t,*), for t = ts_rec(i) 
        uf_i_1 = get_sol(o, sol_path, i);        
        
        for q = -lj:lj              
            j = j0 + q;

            %% get the samples of u^{f_ij}(T,*)            
            uf_i_j = get_windowed_uf_ij(o, uf_i_1, i, j, xinds, yinds);
            SS_i_j = squeeze(uf_i_j(o.mt_rec, :, :));
            
            % imagesc(SS_i_j); 
            % colorbar
            % pause(.1);
                                    
            % So each row has a fixed value of the temporal index,
            % while the columns vary in the spatial variable. Flattening
            % by 
            %
            %               SS_i_j = SS_i_j(:) 
            %
            % turns SS_i_j into a vector with (2l+1)^2 columns and 1 row
            % organized into (2l+1) vectors of size (2l+1) x 1 each
            % of which corresponds to a measurement with fixed spatial
            % coordinate but varying temporal coordinate            
            SS_i_j = SS_i_j(:);                        
            
            % save the samples
            S(:, (2*lj+1)*(p + li) + (q+lj+1))  = SS_i_j;            

        end
    end        
end 