function [Utt, iloc,ihic,jloc,jhic,U2tt] = compute_wvfld_recon_sampling_matrix(o, oc, save_path, S, ...
                                                      ilo,ihi,jlo,jhi)
% o is the bcm discr
% oc is the coarse discr    


    %% get a fine mesh so we can plot the coarse basis functions and
    %% project into the fine basis
    ts_fine = o.ts_rec(1:o.mt_rec);
    xs_fine = o.xs_rec(1:o.nx_rec);
    [XS_fine, TS_fine] = meshgrid(xs_fine, ts_fine);
        
    %% the dilation ratio
    dilation = oc.dilation;

    function ic = get_coarse_i(i)
        if i < o.mx_src
            ic = 1 + ceil((i-1)/dilation);
        elseif i > o.mx_src
            ic = 1 + floor((i+1)/dilation);
        else
            ic = oc.mx_src;
        end
    end

    function jc = get_coarse_j(j)
        jc = 1 + floor((j-1)/dilation);
    end

    iloc = get_coarse_i(ilo)
    ihic = get_coarse_i(ihi)

    jloc = get_coarse_j(jlo)
    jhic = get_coarse_j(jhi)

    %% how many points in the interior we want samples at
    nic = (ihic-iloc)+1
    njc = (jhic-jloc)+1
    nsamples_c = nic * njc

    %% how many basis functions in our basis
    ncoarse_basis = nic * njc

    Utt = zeros(nsamples_c, ncoarse_basis);

    % for testing purposes
    U2tt = zeros(nsamples_c, ncoarse_basis);
    basic_sol = get_basic_sol(o, save_path);
    [XX, YY] = meshgrid(oc.xs_src(iloc:ihic), oc.ts_src(jloc:jhic));
    [xs_sol_padded, ys_sol_padded] = get_basic_sol_padding(o);
    [XX2, YY2] = meshgrid(xs_sol_padded, ys_sol_padded);

    for i = iloc :ihic
        fprintf('%d/%d\n', i, ihic);
        for j = jloc : jhic

            jp = oc.nt_src - (j - jloc);
            
            f_ij_tt_coarse_fun = ...
                (-2 * oc.at + 4 * oc.at^2 * (TS_fine - oc.ts_src(jp)).^2) .*...
                fun_basis_t(oc, TS_fine, jp) .* ...
                fun_basis_x(oc, XS_fine, i);    
            f_ij_tt_integrals = proj_basis(o, f_ij_tt_coarse_fun);
            f_ij_tt_coeffs = o.coeffs_invG * f_ij_tt_integrals;

            % which row to put data in
            k = (i-iloc)*njc + (j-jloc+1);
            
            % perform the wavefield reconstruction
            u_ij = wvfld_recon_sampling(S, f_ij_tt_coeffs);            
            u_ij = reshape(u_ij, jhi - jlo + 1, ihi - ilo + 1);
            u_ij = u_ij(1 : dilation : (jhi - jlo)+1, 1 : dilation : (ihi-ilo)+1);            
            u_ij = u_ij(:);
           
            Utt(:,k) = u_ij;


            % for testing purposes
            plot_sol_from_probes(o, basic_sol, f_ij_tt_coeffs / ...
                                 o.normalization_Lambda(1), o.mt_rec, ...
                                 save_path, 'junk');            
            jj = load([save_path '/junk.mat']);
            ut = (jj.ut)';                
            ut_interp = interp2(XX2, YY2, ut, XX, YY - oc.t_separation / 2);
    
            U2tt(:,k) = ut_interp(:);


        end
    end
    
end
