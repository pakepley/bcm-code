function u = wvfld_recon_sampling(S,  h_coeffs)

    %% how many points in the interior we want samples at
    dims = size(S);
    nsamples = dims(1);
    nbasis = dims(2);
        
    u = zeros(nsamples,1);

    u = S * h_coeffs;
    
end