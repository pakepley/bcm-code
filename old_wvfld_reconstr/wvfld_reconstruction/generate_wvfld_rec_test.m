function f_coeffs = generate_wvfld_rec_test(o, xc, tc, rescale)
%% --------------------------------------------------------------------
%% Function : generate_mvng_rec_rhs(o, t_c, i)
%% Use      : generate rhs for moving receivers. In particular, we generate
%%            coefficients of a Gaussian pulse projected into the basis
%%            with central time t_c, location p_c (in appropriate 
%%            coordinates) and scaling parameter sigma_h.
%% --------------------------------------------------------------------
%% Inputs: 
%% o       - the bcm discretization structure    
%% xc      - center of the source in space
%% tc      - central time for the source
%% rescale - a number used to rescale the source
%% --------------------------------------------------------------------
%% Outputs:
%% f_coeffs - the coeffs of the function
%% --------------------------------------------------------------------
        
    mt_rec = o.mt_rec;    

    if strcmp(o.domain_type, 'disk')
        % [Thetas,Ts] = meshgrid(o.thetas_rec, o.ts_rec(1:mt_rec));
        % theta_c = o.thetas_src(i);
        % f = o.normalization_Lambda(i) * ...
        %     exp(-(o.atheta * (Thetas - theta_c).^2 + o.at *(Ts - t_c).^2));   
    elseif strcmp(o.domain_type, 'half_space')
        [Xs,Ts] = meshgrid(o.xs_rec, o.ts_rec(1:mt_rec));
        f = exp(- (1.0 / rescale) * (o.ax * (Xs - xc).^2 + o.at * (Ts - tc).^2));
    end

    f_integral   =             proj_basis(o,f) ;
    f_coeffs     =  o.coeffs_invG * f_integral ;
    
end
