function [f, psi] = control_into_psi(o, oc, U, i0, jloc, jhic, ...
                                     iloc, ihic, alpha)

    %% how many points in the interior we want samples at
    nic = (ihic-iloc)+1
    njc = (jhic-jloc)+1
    nsamples_c = nic * njc
        
    N_alpha = (U' * U + alpha * eye(nsamples_c, nsamples_c));
        
    % compute the target samples
    psi = (oc.xs_src(iloc : ihic) - oc.xs_src(i0)).^2;
    psi = repmat(psi, njc, 1);
    psi = psi(:);
    
    % solve the data fitting problem (control problem)
    f = gmres(N_alpha, (U' * psi), 20, [], 300); 
    
end