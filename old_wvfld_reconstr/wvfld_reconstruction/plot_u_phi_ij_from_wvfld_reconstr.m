function uu = plot_u_phi_ij_from_wvfld_reconstr(o, S, i,j, ilo, ihi, jlo, jhi)

    %% how many points in the interior we want samples at
    ni = (ihi-ilo)+1
    nj = (jhi-jlo)+1

    l = o.nt_src * (i-1) + j;
    uu = reshape(S(:,l), nj, ni);
    
end