function [next_r_index , is_done] = get_next_r_index(o, j, vol_target, ...
                                                        vol_overlaps, ...
                                                        max_r_index, mode)
%% --------------------------------------------------------------------
%% Function: get_next_r_index(o, j, vol_target, vol_overlaps, ...
%%                            max_r_index, mode)                              
%% Use     : get the next index needed in computing r_h, and inform the
%%           user if we'eve computed everything requested for the
%%           given mode
%% --------------------------------------------------------------------
%% Inputs:
%% o            - the bcm discretization structure
%% j            - which experiment we're working on
%% vol_target   - the target cap volume
%% vol_overlaps - containers which have the overlap volumes computed for
%%                the experiments
%% max_r_index  - the largest ts_src index to be used when computing r_h
%% mode         - a string saying whether we want to compute all volumes
%%                (mode = 'default' or 'all') or just enough volumes
%%                to deduce r_h (mode = 'minimal') 
%% --------------------------------------------------------------------
%% Outputs:
%% next_r_index - the next ts_src index to be used as a value for r(i)
%% is_done      - a flag which is true if we have either:
%%                1) [when  mode ='default' or 'all'] computed all volumes 
%%                   corresponding to r(i) i = 1, ... , max_r_index
%%                2) [when mode ='minimal'] computed enough volumes to
%%                   deduce r_h
%% --------------------------------------------------------------------
    
    n_experiment_containers  = length(vol_overlaps);
    
    %% find the non-empty indicies of vol_overlaps
    nonempty_vol_overlap_inds = find(~cellfun(@isempty,vol_overlaps));    

    %% check if j is one of the non-empty indices
    nothing_computed_for_jth = not( any(nonempty_vol_overlap_inds == j) );
    
    % We need to compute a starting index for the jth set of volumes
    % this depends on the mode we are using, and is estimated from 
    % other volumes we have computed.
    if nothing_computed_for_jth
        % in the default mode, we compute overlap volumes using 
        % EVERY r value so we start at index 1        
        if or(strcmp(mode,'default') , strcmp(mode,'all'))
            next_r_index = 1;

        % if we have not computed any overlap volumes yet, then we
        % must start at index 1 even in the 'minimal' mode
        elseif (n_experiment_containers == 0)
            next_r_index = 1;
        
        % in this last case, we're using the minimal mode and have
        % some containers with overlap volumes. we use these to 
        % guess a starting index
        elseif strcmp(mode,'minimal')
            % loop through the other containers looking for in
            for j_others = 1:(j-1)
                % check if we have data
                if not(isempty(vol_overlaps{j_others}))
                    % if we have data: find the index i_lo for which
                    % r_h falls between r(i_lo) and r(i_lo + 1)
                    [r_h, i_lo] = estimate_r_h(o,vol_target, ...
                                               vol_overlaps{j_others});
                    
                    % if there is no index i_lo for which r_h falls between r(i_lo) and
                    % r(i_lo+1), then i_lo will be returned as an empty cell
                    % struct. if i_lo DOES exist, then we use it as the
                    % candidate for "next_r_index"
                    if not(isempty(i_lo))
                        next_r_index = i_lo;
                    
                     % although we may not have found a candidate, we may
                     % have done SOME computation for another j.                     
                    else 
                        [dummy, ii] = min(abs(cell2mat(vol_overlaps{j_others}) ...
                                              - vol_target));
                        if exist('i_lo','var')
                            next_r_index = max(ii, i_lo);
                        else
                            next_r_index = ii;
                        end
                    end
                end
            end
            
            % we failed to find any candidates, this will happen if all
            % the other containers are actually empty containers
            if not(exist('next_r_index','var'))
                next_r_index = 1;
            end
        end
        
        
        
    % IN this case, we HAVE computed some overlap volumes for the jth position    
    else
        % if we're in the default/all mode, we will be computing ALL 
        if or(strcmp(mode,'default'), strcmp(mode,'all'))            
            n_r_computed = length(vol_overlaps{j});
            missing_data = false; % initially assume we're not missing data
            for ll = 1:n_r_computed
                missing_data = isempty(vol_overlaps{j}{ll}) || ...
                    missing_data;
                if missing_data
                    break
                end
            end
            if (missing_data)
                next_r_index = ll;
            else
                next_r_index = n_r_computed + 1;
            end
        else
            [r_h, i_lo] = estimate_r_h(o, vol_target, vol_overlaps{j});

            % if there is no return value for i_lo, then we haven't
            % computed enough volumes to compute r_h, so there is still
            % work to do          
            if isempty(i_lo)
                for l = 1:length(vol_overlaps{j})
                    if isempty(vol_overlaps{j}{l})
                        vol_overlaps{j}{l} = 0.0;
                    end
                end
                relative_vol_overlaps = cell2mat(vol_overlaps{j}) / vol_target;
                geq_half = find( relative_vol_overlaps >= .5);
                lt_half = find( relative_vol_overlaps < .5);

                % since we can't compute r_h, there we either have all the relative volumes
                % less than half or all the relative volumes greater than
                % one half
                if geq_half
                    next_r_index = geq_half(1) - 1;
                else
                    next_r_index = lt_half(end) + 1;
                end
            else                
                next_r_index = i_lo;
            end
        end
    end

        
    % Now set the flag to inform whether we're done with computations or
    % not
    [r_h, i_lo] = estimate_r_h(o, vol_target, vol_overlaps{j});
    if and(strcmp(mode,'minimal'), isempty(i_lo))
        is_done = false;
    elseif and(or(strcmp(mode,'default'), strcmp(mode,'all')), ...
               next_r_index < max_r_index)
        is_done = false;
    else
        is_done = true;
    end
    

end