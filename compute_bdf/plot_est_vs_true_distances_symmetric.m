function plot_est_vs_true_distances_symmetric(o, distance_path, y, s, plot_to_file_flag)
%% --------------------------------------------------------------------
%% Function: plot_est_vs_true_distances(o, distance_path, y, s)
%% Use     : plot the estimated vs the true distances, save in the
%%           distance path directory
%% --------------------------------------------------------------------
%% Inputs:
%% o             - the bcm discretization structure
%% distance_path - where the distances are saved
%% y             - the base point
%% s             - the inward traveltime depth
%% plot_to_file_flag - whether or not to plot to file
%% --------------------------------------------------------------------

    %% Estimate the distances
    [dd_est, zs] = compute_distances(o, s, distance_path);
    nzs = length(zs);

    %% Compute true distances
    x_y_s = compute_xys(o, y, s);
    qs = [zs', zeros(nzs, 1)];
    dd_true = true_distances(o, x_y_s, qs);

    zs = [-fliplr(zs(2:end)), zs];
    dd_true = [fliplr(dd_true(2:end)), dd_true];
    dd_est = [fliplr(dd_est(2:end)), dd_est];
    
    %% Plot the comparison
    hold on;
    plot(zs,dd_true); 
    scatter(zs, dd_est);
    axis([-1 1 0 1]);
    hold off;
    if plot_to_file_flag
        print('-dpng', [distance_path '/est_vs_true_comp.png']);
    end
    
end