function [distances, zs] = compute_exact_distances(o,save_path, ...
                                                   s,y,z)
%% --------------------------------------------------------------------
%% Function: compute_exact_distances(o, save_path, s, y, z)
%% Use     : compute exact distances and save them to the file,
%%           distance_path/exact_distances.mat
%% --------------------------------------------------------------------
%% Inputs:
%% o             - the bcm discretization structure
%% save_path     - the location where everything is saved
%% distance_path - the work directory where we compute distances
%% i             - the index  of the point y = xs_src[i]
%% s             - the height of the flat portion 
%% --------------------------------------------------------------------
%% Outputs:
%% distances     - the distances we computed, i.e. d(zs, x(y,s))
%% --------------------------------------------------------------------


fprintf(sprintf(['python compute_exact_distances.py ' save_path ' %g %g %g\n'], s,y,z));
system(sprintf(['python compute_exact_distances.py ' save_path ' %g %g %g'], s,y,z));

end