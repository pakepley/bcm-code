function junk_script4(o, dist_root, y, s,  h)
%% --------------------------------------------------------------------
%% Function: generate_data_for_dists
%% Use     : generates data for dist computaiton procedure
%% --------------------------------------------------------------------
%% Inputs:
%% o        - the bcm discretization structure
%% os       - inversion parameters
%% j_lo     - low index for base-point z  (integer between 1 and o.mx_src)
%% j_hi     - high index for base-point z (integer between 1 and o.mx_src)
%% j_stride - how many src points between each basepoint (integer)
%% s        - the height of the flat portion 
%% y        - the target cap base-point
%% h        - h is the target cap height, s+h is the target cap radius
%% --------------------------------------------------------------------
%% Optional Inputs:
%% mode     - a string informing us how to compute the data. By default,
%%            we will sweep through every possible r-value. If mode is
%%            set to 'minimal', then we will only make the minimum volume
%%            computations required to estimate the dists
%% --------------------------------------------------------------------

    
    dist_path = [dist_root sprintf(['/experiment_y_%.03e_s_%.03e_h_%.03e'], ...
                                   y,s,h)];

    
    % file to save the volumes we compute:
    vol_file = [dist_path '/vols.mat'];
    
    % subdirectories to hold bcm solution data
    tau_roots = cell(1,4);
    for i = 1:4
        tau_roots{i} = [dist_path sprintf('/tau%d', i)];        
        if not(exist(tau_roots{i},'dir'))
            mkdir(tau_roots{i});
        end
    end
    
    
    vol1 = load([tau_roots{1} '/data/data.mat']); 
    vol1 = vol1.vol;

    vol2 = load([tau_roots{2} '/data/data.mat']);
    vol2 = vol2.vol;

    vol_target = vol2 - vol1;

    nonempty_3_max = 0;
    for i = 1:o.nx_src
        experiment = sprintf([tau_roots{3} '/experiment_%d'], i);
        if exist(experiment, 'dir')
            data_root = [experiment '/data'];
            for j = 1:o.nt_src
                data_file = sprintf([data_root '/data_%.04d.mat'], j);
                if exist(data_file, 'file')
                    nonempty_3_max = i;
                    vol3 = load(data_file);
                    vol3 = vol3.vol;
                    vols3{i}{j} = vol3;
                end
            end
        end
    end

    nonempty_4_max = 0;
    for i = 1:o.nx_src
        experiment = sprintf([tau_roots{4} '/experiment_%d'], i);
        if exist(experiment, 'dir')
            data_root = [experiment '/data'];
            for j = 1:o.nt_src
                data_file = sprintf([data_root '/data_%.04d.mat'], j);
                if exist(data_file, 'file')
                    nonempty_4_max = i;
                    vol4 = load(data_file);
                    vol4 = vol4.vol;
                    vols4{i}{j} = vol4;
                end
            end
        end
    end

    for i = 1:o.nx_src,
        if i <= min(nonempty_3_max, nonempty_4_max)
            if not(isempty(vols3{i})) && not(isempty(vols4{i}))
                n_mutual_vols = min(length(vols3{i}), length(vols4{i}));
                for j = 1:n_mutual_vols
                    vol_overlaps{i}{j} = vol2 + vols3{i}{j} - vol1 - vols4{i}{j};
                end
            end
        end
    end

    save([dist_path '/vols.mat'], 'vol1', 'vol2', 'vols3', 'vols4', ...
          'vol_target', 'vol_overlaps');
