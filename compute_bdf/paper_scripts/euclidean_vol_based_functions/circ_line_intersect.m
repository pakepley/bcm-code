function [left,right] = circ_line_intersect(y,s,h)
    left  = y - sqrt(h^2 + 2*s*h);
    right = y + sqrt(h^2 + 2*s*h);
end
