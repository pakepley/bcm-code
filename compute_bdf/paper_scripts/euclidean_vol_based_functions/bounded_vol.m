function vol = bounded_vol(y,h,z,r,s)
    if y > z
        bounded_vol(z,r,y,h,s);
    else
        [left_y,right_y] = circ_line_intersect(y,s,h);
        [left_z,right_z] = circ_line_intersect(z,s,r);
        if left_z > right_y
            %fprintf('Case a\n');
            vol = 0;
        elseif left_z <= left_y
            %%compute just Vol(cap(y,s,h))
            %fprintf('Case b\n');
            fun = @(x) circ_ysh(x,y,s,h) - s;
            vol = quad(fun,left_y,right_y);

        elseif right_z <= right_y
            %% compute just Vol(cap(z,s,r))
            %fprintf('Case c\n');
            fun = @(x) circ_ysh(x,z,s,r) - s;
            vol = quad(fun,left_z,right_z);

        else
            %% switches half way through
            %fprintf('Case d\n');
            circ_int = circ_circ_intersect(y,h,z,r,s);
            fun_y = @(x) circ_ysh(x,y,s,h) - s;
            fun_z = @(x) circ_ysh(x,z,s,r) - s;
            vol_left  = quad(fun_z,left_z,circ_int);
            vol_right = quad(fun_y,circ_int,right_y);
            vol = vol_left + vol_right;
        end
    end
end