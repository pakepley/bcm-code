function out = vol_cap(s,r)
  vol_circ = .5 * pi * (r+s)^2;
  alpha =  pi/2 - atan(sqrt(r^2+2*s*r)/s);
  %% volume of rectangle with sides s, sqrt(r^2 + 2*r*s) 
  %% plus volume of sector with angle 2*alpha
  vol_bott = s * sqrt(r^2 + 2*r*s) + alpha * (r + s)^2;
  out = vol_circ - vol_bott;
end