function r = geometric_distortion_r(y,z,s,h,rho)
% --------------------------------------------------------------
% Inputs:
% --------------------------------------------------------------
% y   : base point of target cap
% z   : base point of variable cap
% s   : distance from boundary to cap
% h   : height of cap
% --------------------------------------------------------------
% Optional Input:
% --------------------------------------------------------------
% rho : volume fraction for distance estimate 
%       (rho = .5 is default, if not supplied)
% --------------------------------------------------------------
% Output:
% --------------------------------------------------------------
% r   : the "analytic" estimate for r based on the value of
%       rho supplied
% --------------------------------------------------------------
    
    
    tol = 10^-3;
    
    if not(exist('rho','var'))
        % default value for rho is .5
        rho = .5;
    end

    % area of the target cap 
    m_target_cap = vol_cap(s,h);
    
    % left and right x-coords of the target cap
    [ll,rr] = circ_line_intersect(y,s,h);
    dLL = sqrt((z - ll)^2 + s^2);       %%   d( (z,0), (ll,s) )
    dRR = sqrt((z - rr)^2 + s^2);       %%   d( (z,0), (rr,s) )
    
    if abs(dLL - dRR) < tol
        rLo = 0;
        rHi = h;
    else
        % max and min r values
        rLo = min(dLL,dRR) - s;
        rHi = max(dLL,dRR) - s;
    end
        
    vol_ratio_lo = bounded_vol(y,h,z,rLo,s) / m_target_cap;
    vol_ratio_hi = bounded_vol(y,h,z,rHi,s) / m_target_cap;
    
    % now we estimate the value of r where the overlap area equals
    %    rho * area of the target cap
    % we estimate r by the bisection method
    
    not_done = true;    
    while not_done
        
        r = rLo + .5 * (rHi - rLo);
        vol_ratio = bounded_vol(y,h,z,r,s) / m_target_cap;
        if vol_ratio <= rho
            rLo = r;
        else
            rHi = r;
        end
        not_done = (abs(vol_ratio - rho) > tol);                
    end       
end