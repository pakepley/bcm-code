function z = circ_ysh(x,y,s,h)
   z = sqrt((s+h)^2 - (x - y).^2);
   z = real(z);
end
