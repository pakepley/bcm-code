coarseness = 'fine';
src_dir = pwd();
h = .025;
ss = [.125,.25,.375,.500];

point_protectors = {'o', 's', 'd', '^'};
point_colors = {[1 0 0], [0 1 0], [0 0 1], [1 1 1]};

figure(1);
clf(1);
hold on;


if not(exist('../images','dir'))
    mkdir('../images');
end

for i = 1:length(ss)
  s = ss(i);
  save_path = sprintf(['~/Dropbox/bcm_project/experiment_images/experiments_s_%.2e/experiment_' coarseness '_h_%.1e'],s,h);
  fprintf([save_path '\n']);
  
  zs_plot = linspace(0,1.1,100);
  r_plot = sqrt( (zs_plot - 0).^2 + s^2);
  
  if exist(save_path, 'dir')
      if exist(sprintf([save_path '/distances_s_%g_h_%g.mat'],s,h))          

          % Load the data, all we need is zs!
          load(sprintf([save_path '/distances_s_%g_h_%g.mat'],s,h));
          fprintf('Loaded zs s = %g h = %g\n',s,h);

          rs_rho = zeros(size(zs));
          for j = 1:length(zs)
              rs_rho(j) = geometric_distortion_r(0.0, zs(j), s,h, .5);
          end          
          
          figure(1);
          axis([-1.0 1.0 0 1]);
          plot( zs_plot, r_plot, 'Color', [0 .5 .5], 'LineWidth', 2.0);
          plot(-zs_plot, r_plot, 'Color', [0 .5 .5], 'LineWidth', 2.0);
          scatter( zs, s + rs_rho, 49, [0 0 1], point_protectors{i}, 'LineWidth', 1.5);%%, point_colors{i});
          scatter(-zs, s + rs_rho, 49, [0 0 1], point_protectors{i}, 'LineWidth', 1.5);%%, point_colors{i});

      else
          fprintf('Still need to run script for s = %g h = %g\n',s,h);
      end
  else
      fprintf('Data directory does NOT exist, s = %g, h = %g\n',s,h);
  end 
  
end

hold off;

figure(1);
set(gca,'fontsize', 15);
%% print without title and labels
print('-dpng', [output_path '/geometric_distortion.png']);
%% print with labels
xlabel('z');
ylabel('d(x(y,s), (z,0))');
print('-dpng', [output_path '/geometric_distortion_no_title.png']);
%% print with title
title('True distances d(x(y,s), (z,0)) and distance estimates');
print('-dpng', [output_path '/images/geometric_distortion_no_labels.png']);
