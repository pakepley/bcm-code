function plot_geometric_distortion_for_paper(save_root, output_path)
%% --------------------------------------------------------------------
%% Function: plot_distance_curves_for_paper(save_root, output_path)
%% Use     : plots distance curves in Euclidean and Hyperbolic cases
%% --------------------------------------------------------------------
%% Inputs:
%% save_root    - the root directory for the data. we assume that
%%                within this directory there are two subdirectories
%%                      save_root / EUCLIDEAN
%%                      save_root / HYPERBOLIC
%%                where the corresponding data is saved
%% output_path  - where we want our plots to go
%% --------------------------------------------------------------------

    y = 0;
    h = .025;
    ss = {.125, .25, .375, .5};
    point_protectors = {'o', 's', 'd', '^'};

    geometry_types = {'EUCLIDEAN'};
    
    for j = 1:length(geometry_types)
        close all;

        save_path = [save_root '/' geometry_types{j}];
        geometry_type = geometry_types{j};
        load([save_path '/discr.mat']);
        dist_root = [save_path '/distances'];

        for i = 1:4            
            s = ss{i};
            marker_shape = point_protectors{i};

            dist_path = [dist_root ...
                         sprintf(['/experiment_y_%.03e_s_%.03e_h_%.03e'], ...
                                 y,s,h)];

            if exist(dist_path, 'dir') && exist([dist_path '/vols.mat'], ...
                                                 'file')

                %% Check to see if there is data to plot, and plot if there is!
                [dd_est, zs] = compute_distances(discr, s, dist_path);
                if not(isempty(zs))
                    fprintf(sprintf([discr.c_code ' %f %f %d\n'], y, s, ...
                                    length(zs)));
                   
                    figure(1); 
                    clf();
                    plot_geometric_distortion(discr, dist_path, y, s, h, ...
                                              true, marker_shape)
                    print('-dpng', sprintf([output_path '/geometric_distortion_%.03f_' ...
                                        geometry_type '.png'], s));
                
                    figure(2);
                    hold on;
                    plot_geometric_distortion(discr, dist_path, y, s, h, ...
                                              true, marker_shape)
                    print('-dpng', sprintf([output_path '/geometric_distortion_' ...
                                        geometry_type '.png'], s));
                end
            end
        end
    end
end
