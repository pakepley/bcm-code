function make_distance_images(o, save_path, distance_path)
%% --------------------------------------------------------------------
%% Function: make_distance_images(o, distance_path)
%% Use     : generates data for distance computaiton procedure
%% --------------------------------------------------------------------
%% Inputs:
%% o        - the bcm discretization structure
%% j_lo     - low index for base-point z  (integer between 1 and o.mx_src)
%% j_hi     - high index for base-point z (integer between 1 and o.mx_src)
%% j_stride - how many src points between each basepoint (integer)
%% s        - the height of the flat portion 
%% y        - the target cap base-point
%% h        - h is the target cap height, s+h is the target cap radius
%% --------------------------------------------------------------------
                
    for i = 1:4
        tau_roots{i} = [distance_path sprintf('/tau%d', i)];                
    end

    % read the basic solution for plotting purposes
    basic_sol_path = sprintf([save_path '/sol/sol_%d.txt'], ...
                             o.mx_src)
    sol = dlmread(basic_sol_path);   
    
    % Set up directory structure for the flat and target taus and solve
    % the bc problem    
    for i = 1:2
        data_path  = [tau_roots{i} '/data'];
        data_file  = [data_path '/data.mat'];
        image_path = [tau_roots{i} '/images'];
        conditional_mkdir(image_path);
        
        %% what the file will be called
        if i == 1
            file_name = 'flat_domain';
        elseif i == 2
            file_name = 'target_cap';
        end
        image_file = [image_path '/' file_name '.png'];
        mat_file = [image_path '/' file_name '.mat'];
        if not(exist(image_file, 'file'))
            if exist(mat_file,'file')
                %% plot from the mat_file
                load(mat_file);
                plot_sol_from_ut(o, ut, save_path, image_path, ...
                                 file_name);
            else
                %% plot and make the mat_file
                load(data_file);                  % <- Loads: vol, f, mask
                plot_sol_from_probes(o, sol, f, o.mt_rec, ...
                                     image_path, file_name);            
            end
        end
    end

    for i = 3:4
        %% Find all of the sub-directories.. not including '.' and '..'
        dd = dir(tau_roots{i});
        dd = dd(arrayfun(@(x) x.name(1), dd) ~= '.');
        tau_sub_dirs{i} = dd;      
    end
    
    for i = 3:4,
        for sd = tau_sub_dirs{i}'
            disp(['Working with ' sd.name]);
            sub_dir  = [tau_roots{i} '/' sd.name];
            data_dir = [sub_dir '/data'];
            image_dir = [sub_dir '/images'];
            if not(exist(image_dir, 'dir'))
                mkdir(image_dir);
            end
            %% find all of the data files          
            dfs = dir(data_dir);
            the_data_files = dfs(arrayfun(@(x) x.name(1), dfs) == 'd');
            for ff = the_data_files'
                data_file = [data_dir '/' ff.name];
                data_index = sscanf(ff.name, 'data_%d.mat');
                if i == 3
                    image_root_name = 'variable_cap';
                elseif i == 4
                    image_root_name = 'combined';
                end
                file_name = sprintf([image_root_name '_%.04d'], ...
                                    data_index);
                image_file = [image_dir '/' file_name '.png'];
                mat_file = [image_dir '/' file_name '.mat'];
                if exist(mat_file,'file')
                    %% plot from the mat_file
                    load(mat_file);
                    plot_sol_from_ut(o, ut, save_path, image_dir, ...
                                     file_name);
                else
                    %% plot and make the mat_file
                    fprintf(['Plotting from ' data_file '\n']);
                    load(data_file);                 % <- Loads: vol, f,
                                                     % mask
                    plot_sol_from_probes(o, sol, f, o.mt_rec, ...
                                         image_dir, file_name);            
                end
            end
        end      
    end
end