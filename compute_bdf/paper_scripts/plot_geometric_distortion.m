function plot_geometric_distortion(o, distance_path, y, s, h, plot_to_file_flag, ...
                                    marker_shape)
%% --------------------------------------------------------------------
%% Function: plot_est_vs_true_distances(o, distance_path, y, s)
%% Use     : plot the estimated vs the true distances, save in the
%%           distance path directory
%% --------------------------------------------------------------------
%% Inputs:
%% o             - the bcm discretization structure
%% distance_path - where the distances are saved
%% y             - the base point
%% s             - the inward traveltime depth
%% plot_to_file_flag - whether or not to plot to file
%% --------------------------------------------------------------------
%% Optional Inputs:
%% marker_shape  - what plot marker the scatter plot should use
%% --------------------------------------------------------------------

    %% Get the points zs
    [~, zs] = compute_distances(o, s, distance_path);
    r_geom = zeros(1,length(zs));
    rho = .5;
    for i = 1:length(zs)
        i
        r_geom(i) = geometric_distortion_r(y,zs(i),s,h,rho);
    end
    dd_geom = s + r_geom;
    
    %% Points to compare with
    zz = linspace(min(zs), max(zs), 1000);
    nzz = length(zz);

    %% Compute true x(y,s)
    x_y_s = compute_xys(o, y, s);

    %% Get the points to compare with:
    qq = [zz', zeros(nzz, 1)];
    dd_true = true_distances(o, x_y_s, qq);

    %% If translation invariant and we have only computed for
    %% zs =< 0 or just zs >=0, use translation invariance to flip
    if o.transl_invar_flag && (min(zs) >= 0.0 || max(zs) <= 0.0)
        zz = [-fliplr(zz(2:end)), zz];
        dd_true = [fliplr(dd_true(2:end)), dd_true];

        zs = [-fliplr(zs(2:end)), zs];
        dd_geom = [fliplr(dd_geom(2:end)), dd_geom];
    end

    %% Default to using circle for marker
    if not(exist('marker_shape', 'var'))
        marker_shape = 'o';
    end

    %% Plot the comparison
    plot(zz, dd_true, 'Color', [0 .5 .5], 'LineWidth', 2.0); 
    hold on;
    scatter(zs, dd_geom, 49, [0 0 1], marker_shape, 'LineWidth', 1.5);    
    hold off;
    axis([-1 1 0 1.15]);

    %% Label the axes
    set(gca,'fontsize', 20);
    xlabel('z');
    ylabel('d( (z,0) , x(y,s) )');


    %% Plot to file if requested
    if plot_to_file_flag
        print('-dpng', [distance_path '/geom_dist_vs_true_comp.png']);
    end
    
end