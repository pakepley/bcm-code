function plot_boundary_function_from_yy(o, yy, output_path, file_name, color_axis, axis_vector, ...
                                font_size)
    
    %% We want the xrange to coincide with the solution figure xrange,
    %% and that is computed as follows:
    %%
    %% The soln was recorded on o.xs_sol, but this may not be enough points
    if abs(o.xs_sol(1) - o.xs_rec(1)) < .1 * o.sol_res
        %% then we only recorded in \Gamma x [0, T]
        nxlnew = ceil((o.xs_sol(1) - o.X0) / o.sol_res) ;
        nxrnew = ceil((o.X1 - o.xs_sol(end)) / o.sol_res) ;
        xs_lo  = o.xs_sol(1)   - nxlnew * o.sol_res;
        xs_hi  = o.xs_sol(end) + nxrnew * o.sol_res;
        xs_sol = xs_lo : o.sol_res : xs_hi;
    else
        %% then we only recorded in \Gamma x [0, T]
        nxlnew = 0;
        nxrnew = 0;
        warning('double check that xs_sol is what you want!');
        xs_sol = o.xs_sol;
        xs_lo = xs_sol(1);
        xs_hi = xs_sol(end);
    end
        
    indT = o.mt_rec ;
    Ntb = length(o.ts_src);
    Nxb = length(o.xs_src);

    %% which points (x,t) to plot?
    x = xs_lo : o.dx : xs_hi;
    t = o.ts_rec(1:indT);
    [xx,tt] = meshgrid(x,t);
        
    hFig = surf(xx,tt,yy);
    shading interp;
    view(0,90);

    if exist('axis_vector', 'var')
        axis(axis_vector);
    else
        axis equal; 
        axis tight;
    end
    colorbar;
    if exist('color_axis', 'var')
        caxis(color_axis);      
    end
    if exist('font_size', 'var')
        set(gca,'FontSize', font_size);
    end
    if exist('file_name', 'var')
        save([output_path '/' file_name '.mat'], 'yy');
        print('-dpng', [output_path '/' file_name '.png']);
    end
end 