function b_integral = generate_rhs_for_bdf(o)
%% --------------------------------------------------------------------
%% Function: generate_b_integral()
%% Use:      compute the inner product between the function 
%%           b(t,x) = T - t and f_ij for each basis function f_ij
%% --------------------------------------------------------------------
%% Inputs:
%% o          - the bcm discretization structure
%% --------------------------------------------------------------------
%% Outputs:
%% b_integral - the integrals of the funbtion b(t,x) = T - t 
%% --------------------------------------------------------------------

    % evaluate the function b(t,x) where we need it:
    b_tmp = (o.T - o.ts_rec(1:o.mt_rec))';                     
    b_tmp = repmat(b_tmp,1,o.nx_rec);
    
    % compute the integrals of b against the basis:
    b_integral = proj_basis(o, b_tmp);
 
end