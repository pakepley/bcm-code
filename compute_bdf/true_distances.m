function dd_true = true_distances(o,p,qs)
%% --------------------------------------------------------------------
%% Function: true_distances(o,p,qs)
%% Use     : compute the true distance between a fixed point p
%%           and multiple points qs
%% --------------------------------------------------------------------
%% Inputs:
%% o    - the bcm discretization structure
%% p    - a fixed point which we want to compute the distance to
%%        should be a 1 x 2 vector
%% qs   - an n x 2 array, each row of which is a point we would like
%%        to compute the distance to.
%% --------------------------------------------------------------------
%% Outputs:
%% dd_true - the true distances d(p,q) for q in qs. returns a row
%%           vector of size 1 x n (!!!!)
%% --------------------------------------------------------------------

    qs_dims = size(qs);
    n_qs    = qs_dims(1);
            
    dd_true = zeros(1, n_qs);

    if strcmp(o.c_code, '1.0')
        for i = 1:n_qs
            dd_true(i) = norm( qs(i,:) - p ); 
        end
    elseif strcmp(o.c_code, '1 + x[1]')
        for i = 1:n_qs
            dd_true(i) = acosh( 1 + ( (p(1,1)-qs(i,1))^2 + (p(1,2)-qs(i,2))^2 ...
                                      ) / (2 * (1+p(1,2)) * (1+qs(i,2))));
        end        
    else
        error(['true_distances.m: ' ...
               'Wave-speed c_code not found. If expression for distances ' ...
               'is known, please check the expression used for c_code vs ' ...
               'the choices understood in this file. If it is not present, ' ...
               'either update this file or you will possibly need to ' ...
               'implement a distance approximation via raytracing or some '...
               'other method. Aborting.']);
    end
end