function [distances, zs] = compute_distances(o, s, distance_path)
%% --------------------------------------------------------------------
%% Function: compute_distances(o, s, distance_path)
%% Use     : compute distances and save them to the file,
%%                 distance_path/distances.mat
%% --------------------------------------------------------------------
%% Inputs:
%% o             - the bcm discretization structure
%% s             - the height of the flat portion 
%% distance_path - the work directory where we compute distances
%% --------------------------------------------------------------------
%% Outputs:
%% distances     - the distances we computed, i.e. d(zs, x(y,s))
%% zs            - the points where the distances were computed
%% --------------------------------------------------------------------
    
    load([distance_path '/vols.mat']);  % <- Should contain vol_overlaps and
                                        %    vol_target

    r_h = cell(1, o.nx_src);            % allocate a cell struct to hold r_h's
    zs  = cell(1, o.nx_src);            % allocate a cell struct to hold zs's

    for j = 1:length(vol_overlaps)
        % find the j where we actually have volume data
        if not(isempty(vol_overlaps{j}))
            r_h_j = estimate_r_h(o, vol_target, vol_overlaps{j});            
            % find the j where we actually can compute r_h
            if not(isempty(r_h_j))
                r_h{j} = r_h_j;
                zs{j}  = o.xs_src(j);
            end
        end
    end
           
    r_h = cell2mat(r_h);
    zs  = cell2mat(zs);    
    distances = s + r_h;
    
    save([distance_path '/distances.mat'], 'zs', 'r_h');
end