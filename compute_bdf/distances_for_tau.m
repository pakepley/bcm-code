function dd = distances_for_tau(o,y)
%% --------------------------------------------------------------------
%% Function: distances_for_tau(o,y)
%% Use     : compute the distances d((y,0) , (x_i,0)) for 
%%           x_i = o.xs_src(i)
%% --------------------------------------------------------------------
%% Inputs:
%% o     - the bcm discretization structure
%% y     - the y-coordinate of the starting point (y,0)
%% --------------------------------------------------------------------
%% Outputs:
%% dd    - the distances dd(i) = d((y,0), (x(i),0)) for x = o.xs_src(i)
%% --------------------------------------------------------------------

    p_y = [y, 0.0];
    qs  = [o.xs_src', zeros(o.nx_src,1)];
    dd = true_distances(o, p_y, qs);

end