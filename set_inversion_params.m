function os = set_inversion_params(Nouter, Nrestart, alpha, tolerance)
%% --------------------------------------------------------------------
%% Function: set_inversion_params(Nouter, Nrestart, alpha, tolerance)
%% Use:      set up the inversion parameters for GMRES and Tikhonov reg
%% --------------------------------------------------------------------
%% Inputs:
%% Nouter        - the number of outer GMRES steps
%% Nrestart      - the number of inner GMRES steps
%% toleratnce    - GMRES tolerance
%% alpha         - the Tihonov regularization parameter    
%% --------------------------------------------------------------------
%% Outputs:
%% os            - the inversion paramters
%% --------------------------------------------------------------------    
    os = struct();

    os.Nouter    = Nouter;
    os.Nrestart  = Nrestart;
    os.alpha     = alpha;
    os.tolerance = tolerance;
end