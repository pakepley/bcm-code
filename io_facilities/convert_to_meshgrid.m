function convert_to_meshgrid(o, save_path, file_name, type)

python_version = '~/anaconda2/bin/python2';
io_facilities_path = '~/git-repositories/bcm-code/io_facilities';

pvtu_file = [save_path '/' file_name '000000.pvtu'];
uninterp_matfile = [save_path '/' file_name '.mat'];
interp_matfile = [save_path '/' file_name '_interp.mat'];
    
if not(exist(uninterp_matfile, 'file'))
    %% first we need to parse the pvtu file to get data
    system([python_version ' ' ...
            io_facilities_path '/parse_vtu.py ' ...
            pvtu_file]);
    system(['mv ' save_path '/' file_name '000000.mat ' ...
            save_path '/' file_name '.mat']);
end

if not(exist(interp_matfile, 'file'))
    %% next we need to interoplate onto a mesh
    system([python_version ' ' ...
            io_facilities_path '/convert_to_meshgrid.py ' ...
            uninterp_matfile ' ' ...
            type]);
end

end