function conditional_mkdir(directory_path)
    if not(exist(directory_path, 'dir'))
        mkdir(directory_path);
    end
end