function out = timestamp() 
%% --------------------------------------------------------------------
%% Function: timestamp
%% Use:      handy function to compute a timestamp
%% --------------------------------------------------------------------
%% Output:
%% out    - the current time stamp
%% --------------------------------------------------------------------
    out = sprintf('%04.0f_%02.0fm_%02.0fd_%02.0f%02.0f%02.0f', ...
                  datevec(now));
end