import os, sys

# This module is a single purpose script that parses a collection of directories 
#     root_path / solution_type_TIME     where TIME = t1,t2,...,tn
# or:
#     root_path / solution_typeTIME     where TIME = t1,t2,...,tn
#
# Each directory possesses a file called:
#     coeff_name000000.pvtu
#
# It writes a pvd file:
#     root_path / coeff_name.pvd
#
# which essentially just wraps the contents of the pvtu files together


def parse_pvd(root_path, solution_type):
	print '{0}/{1}.pvd'.format(root_path,solution_type)
	the_file = open('{0}/{1}.pvd'.format(root_path,solution_type), 'r')
	file_lines = list(the_file)
	the_file.close()

	pvtu_files = []
	
	for line in file_lines:
		line_pieces = line.split(' ')
		ff = [x for x in line_pieces if 'file=' in x]
		if ff:
			#file_name = ff[0].split('/')[1].strip('"')
			file_name = ff[0].split('=')[1].strip('"')
			pvtu_files.append(file_name)

	return(pvtu_files)

# ff = '<?xml version="1.0"?>\n<VTKFile type="Collection" version="0.1">\n  <Collection>\n'
# for dd in dirs:
# 	# strip off the solution_type from the directory. This will
# 	tt = dd.strip(solution_type)
# 	tt.strip('_')
# 	ff = ff + '    <DataSet timestep="{0}" part="0" file="./{1}/{2}000000.pvtu" />\n'.format(tt,dd,coeff_type)
# ff = ff + '  </Collection>\n</VTKFile>\n'

if __name__ == '__main__':
	root_path = sys.argv[1]
	pvd_name = sys.argv[2]
	parse_pvd(root_path, pvd_name)
	
