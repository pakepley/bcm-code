import os, sys

# This module is a single purpose script that parses a collection of directories 
#     root_path / solution_type_TIME     where TIME = t1,t2,...,tn
# or:
#     root_path / solution_typeTIME     where TIME = t1,t2,...,tn
#
# Each directory possesses a file called:
#     coeff_name000000.pvtu
#
# It writes a pvd file:
#     root_path / coeff_name.pvd
#
# which essentially just wraps the contents of the pvtu files together

root_path = sys.argv[1]
solution_type = sys.argv[2]
coeff_type = sys.argv[3]

dirs = [dd for dd in os.listdir(root_path) if solution_type in dd]
dirs.sort()
print dirs

ff = '<?xml version="1.0"?>\n<VTKFile type="Collection" version="0.1">\n  <Collection>\n'
for dd in dirs:
	# strip off the solution_type from the directory. This will
	tt = dd.strip(solution_type)
	tt.strip('_')
	ff = ff + '    <DataSet timestep="{0}" part="0" file="./{1}/{2}000000.pvtu" />\n'.format(tt,dd,coeff_type)
ff = ff + '  </Collection>\n</VTKFile>\n'
	
the_file = open('{0}/{1}.pvd'.format(root_path,coeff_type), 'w')
the_file.write(ff)
the_file.close()
