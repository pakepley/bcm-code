function [basic_sol, xs_sol, ys_sol] = pad_basic_sol(o, basic_sol)
%% --------------------------------------------------------------------
%% Function: pad_basic_sol(o, basic_sol)
%% Use:      zero pad a basic solution
%% --------------------------------------------------------------------
%% Inputs:
%% o         - the bcm discretization structure
%% basic_sol - the basic solution centered at mx_src
%% --------------------------------------------------------------------
%% Outputs:
%% basic_sol - the zero padded basic solution
%% xs_sol    - the zero padded coordinates
%% ys_sol    - the zero padded coordinates
%% --------------------------------------------------------------------
    
    %% Get padded coordinates
    [xs_sol, ys_sol, nxlnew, nxrnew] = get_basic_sol_padding(o);
    
    %% Apply zero padding if needed
    if nxlnew > 0 || nxrnew > 0
        tmp = zeros(o.mt_rec, o.nx_sol + nxlnew + nxrnew, o.ny_sol);
        tmp(:, (nxlnew + 1) : (nxlnew + o.nx_sol), :) = basic_sol;
        basic_sol = tmp;
    end
    
end
