function [rhs_integral, f_coeffs] = generate_mvng_rec_rhs(o, t_c, i)
%% --------------------------------------------------------------------
%% Function : generate_mvng_rec_rhs(o, t_c, i)
%% Use      : generate rhs for moving receivers. In particular, we generate
%%            coefficients of a Gaussian pulse projected into the basis
%%            with central time t_c, location p_c (in appropriate 
%%            coordinates) and scaling parameter sigma_h.
%% --------------------------------------------------------------------
%% Inputs: 
%% o       - the bcm discretization structure    
%% t_c     - central time for the source
%% i       - index of target source in space
%% --------------------------------------------------------------------
%% Outputs:
%% rhs_integral - the integrals of Kf against the basis functions
%% --------------------------------------------------------------------
        
    mt_rec = o.mt_rec;    

    if strcmp(o.domain_type, 'disk')
        [Thetas,Ts] = meshgrid(o.thetas_rec, o.ts_rec(1:mt_rec));
        theta_c = o.thetas_src(i);
        f = o.normalization_Lambda(i) * ...
            exp(-(o.atheta * (Thetas - theta_c).^2 + o.at *(Ts - t_c).^2));
        
    elseif strcmp(o.domain_type, 'half_space')
        [Xs,Ts] = meshgrid(o.xs_rec, o.ts_rec(1:mt_rec));
        x_c = o.xs_src(i);
        f = o.normalization_Lambda(i) * ...
            exp(-(o.ax * (Xs - x_c).^2 + o.at * (Ts - t_c).^2));
    end

    f_integral   =             proj_basis(o,f) ;
    f_coeffs     =  o.coeffs_invG * f_integral ;
    rhs_coeffs   = o.coeffs_invGK *   f_coeffs ;
    rhs_integral =     o.coeffs_G * rhs_coeffs ;
    
end
