"""
  Modification of Douglas Arnold's code.
  PAK: M:04/D:22/Y:14
  LSO: M:04/D:24/Y:14
  LSO: M:04/D:29/Y:14

  Domain: \Omega = (-X,X) \times (0,Y) 

  Newmark solver for the wave equation with Neumann boundary conditions:

    u_tt - Lap u = F        in          \Omega \times (0,2T)
    D_n u = 0               on \partial \Omega \times (0,2T)
    u = 0 for t negative enough  

  Source F is a gaussian pulse centered at (xc,yc) in space and
  centered on ts_src[i] in time. 

"""

from dolfin import *

from consolidate_probes import *

#the following is used to speed up pointwise evals
from numpy import *
from fenicstools.Probe import * #used to handle parallel file IO
import mpi4py

comm = mpi4py.MPI.COMM_WORLD
rank = comm.Get_rank()
	
# essentially a wrapper for print. only print on
# process zero.
def prettier_print(s):
	if rank == 0:
		print(s)

# File i/o (no mathematics here)
import sys, os

def redirect_stdout():
    prettier_print("Redirecting stdout")
    sys.stdout.flush() # <--- important when redirecting to files

    # Duplicate stdout (file descriptor 1)
    # to a different file descriptor number
    newstdout = os.dup(1)

    # /dev/null is used just to discard what is being printed
    devnull = os.open('/dev/null', os.O_WRONLY)

    # Duplicate the file descriptor for /dev/null
    # and overwrite the value for stdout (file descriptor 1)
    os.dup2(devnull, 1)

    # Close devnull after duplication (no longer needed)
    os.close(devnull)

    # Use the original stdout to still be able
    # to print to stdout within python
    sys.stdout = os.fdopen(newstdout, 'w')

# to avoid IO repetition in parallel
# redirect_stdout()

# sometimes I use the '-B' flag, but it throws off my the
# length of argv below. just get rid of it
if '-B' in sys.argv:
	sys.argv.pop(sys.argv.index('-B'))
# get the data path
data_path = sys.argv[1]
sys.path.append(data_path)

from int_femparams import *

#print "Loaded femparams with int_save_path %s" % int_save_path
prettier_print("Loaded femparams with int_save_path " + data_path)

#check if directories exist to hold files
if 'sol' not in os.listdir(data_path):
	os.mkdir(data_path+'/sol')

#set up directories to hold the other IO
sol_root = int_save_path
sol_path = sol_root + '/sol'
if not os.path.exists(sol_path):
    os.makedirs(sol_path)
sol_file = File('{0}/{1}.pvd'.format(sol_path, 'sol'))

trace_root = int_save_path
trace_path = trace_root + '/trace'
if not os.path.exists(trace_path):
    os.makedirs(trace_path)

# probes to recod actual boundary trace
trace_ps = array([[x,0] for x in xs_rec])

# copy of ts_src which we will modify with process frame
ts_snpsht_to_record = ts_snpsht[:]

plotframes = False
plotfreq = 10
nPrint = 0
def processframe(j, t, u):
    # use the global ts_snpsht_to_record. we will pop off the first entry
    # if t == ts_snpsht_to_record[0]
    global ts_snpsht_to_record
    
    if t > -0.1*dt: 
        prettier_print("  saving t = {0}".format(t))
        trace_probes(u)
    if abs(ts_snpsht_to_record[0] - t) < .1 * dt:
        t_cur_snpsht = ts_snpsht_to_record.pop(0)
        prettier_print("Recording snapshot at time {0}".format(t_cur_snpsht))
        sol_file << u
    if (j % plotfreq) == 0 and plotframes:
        plot(u)

# Source 
code = '''
  #include <cmath>
  #include <dolfin/function/Function.h>

  namespace dolfin{

  class Gaussian : public Expression {  

    public:
      double t;        // time    
      double ax;       // spatial gaussian factor
      double at;       // temporal gaussian factor
      double xc;       // center of Gaussian in space, x
      double yc;       // center of Gaussian in space, y
      double tc;       // center of Gaussian in time

      Gaussian() : Expression() {}

      void eval(Array<double>& values, const Array<double>& x) const {
        values[0] = exp(-ax*((x[0]-xc)*(x[0]-xc) + (x[1]-yc)*(x[1]-yc))) * exp(-at*(t-tc)*(t-tc));
      }
    };
  }
'''

F = Expression(code, 
			   ax = ax,
			   at = at,
			   xc = xc,
			   yc = yc,
			   tc = tc)

#
# Newmark method over FEniCS 
#
# Initialize the mesh:
dolfin_version = int(dolfin.__version__.split('.')[1])
if dolfin_version < 6: 
	mesh = RectangleMesh(X0,0.,X1,Y, nx,ny, "left/right")
else:
	mesh = RectangleMesh(Point(X0,0.), Point(X1,Y), nx,ny, "left/right")

# Initialize function space and basis functions on mesh:
deg = 1
V = FunctionSpace(mesh, 'CG', deg)

#probes to do the function evaulation, they require V
trace_probes = Probes(trace_ps.flatten(),V)

# Define Functions on V:
u = TrialFunction(V)     # function form for solution
phi = TestFunction(V)    # function form for test function

# Define the sound-speed from 'c_code.' Included from wave_speed.py
c = Expression(c_code)

#   print "Assembling mass matrix"
prettier_print("Assembling mass matrix")
M = assemble(c**(-2) * u * phi *dx)

#   print "Assembling stiffness matrix"
prettier_print("Assembling stiffness matrix")
A = assemble(inner(grad(u),grad(phi))*dx)

# Matrices used in time-stepping
B =      M + dt**2*A/4.0
C =  2.0*M - dt**2*A/2.0
D = -1.0*M - dt**2*A/4.0

# Initially everything vanishes
F0 = interpolate(Constant(0.0), V)
Fcurvec = assemble(c**(-2) * F0 * phi * ds) 
Foldvec = Fcurvec
ucur = interpolate(Constant(0.0), V)
uold = interpolate(Constant(0.0), V)
unew = Function(V)


# If we do NOT provide an initial time margin,
# then we immediately start recording, so we need
# to handle this possibility
if nt_margin == 0:
	# save t = 0
	processframe(0,0,uold)
	# save t = dt
	processframe(1,dt,ucur)

prettier_print("Timestepping")
timer = Timer("Time step");
for j in range(2-nt_margin, nt ):
    timer.start();
    F.t = j*dt
    Fnewvec = assemble(c**(-2) * F * phi * dx)
    L = C*ucur.vector() + D*uold.vector() + dt**2 * (Foldvec + 2.0 * Fcurvec + Fnewvec)/4.0

    solve(B, unew.vector(), L, "cg") # TODO could we use preconditioning?
      
    uold.assign(ucur)
    ucur.assign(unew)
    Foldvec = Fcurvec
    Fcurvec = Fnewvec
    timer.stop();

    processframe(j, F.t, ucur)
    if F.t > -0.1*dt: 
        nPrint += 1
        prettier_print("Time = {0} (out of {1}), step {2}/{3} done".format(F.t, bigT, j, nt - 1))


# dump the output to file
trace_probes.dump(int_save_path + '/trace/trace')

# consolidate the probes
mpi4py.MPI.COMM_WORLD.Barrier()
if rank == 0:
	prettier_print('Consolidating trace probes')
	consolidateProbes(trace_root, 'trace', 'trace')

