function S = build_sampling_matrix(o, sol_path, xs, ys)
    nxs = length(xs);
    nys = length(ys);
    Nrows = nxs * nys;
    
    Ncols = o.nx_src * o.nt_src;
    
    S = zeros(Nrows, Ncols);
    
    for i = 1:o.nx_src
        i
        % get u_i_1 measured at (ts_sol, xs_sol, ys_sol)
        u_i_1 = get_sol(o, sol_path, i);
        
        for j = 1:o.nt_src
            % Delay the solution by (j-1) t_separations:
            indt = o.mt_rec - o.ratio_t_separation_dt * (j - 1);
            u_i_j_T = u_i_1(indt,:,:);
            S(:, o.nt_src * (i -1) + j) = u_i_j_T(:);
        end
    end
    
end