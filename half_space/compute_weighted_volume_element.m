function [dV_g_mu,Xs,Ys] = compute_weighted_volume_element(o, save_path, output_path, Xs, Ys)
%% --------------------------------------------------------------------
%% Function: compute_weighted_volume_element
%% Use:      compute the weighted volume element at given points
%% --------------------------------------------------------------------
%% Inputs:
%% o           - the bcm discretization structure
%% save_path   - where everything is saved
%% output_path - where the matfile containing the weighted volume 
%%               element will be saved
%% Xs          - the x-coordinates of the points where we want to
%%               measure the weighted volume element
%% Ys          - the y-coordinates of the points where we want to
%%               measure the weighted volume element
%% --------------------------------------------------------------------
%% Outputs:
%% dV_g_mu     - the weighted volume element evaluated at the points
%%               p(i) = (Xs(i),Ys(i))
%% Xs,  Ys     - the x(resp. y)-coordinates of the points where we want 
%%               to measure the weighted volume element. Should be the
%%               same as the Xs and Ys in 
%% --------------------------------------------------------------------
    
    volume_weight_matfile = [output_path '/dV_g_mu.mat'];
    Xs = Xs(:);
    Ys = Ys(:);
    save(volume_weight_matfile, 'Xs', 'Ys', '-v7');
    system(['python ./half_space/compute_weighted_volume_element.py ' ...
            save_path ' ' volume_weight_matfile ' -B']);
    
    a = load(volume_weight_matfile);
    dV_g_mu = a.dV_g_mu;
    Xs      = a.Xs;
    Ys      = a.Ys;
end