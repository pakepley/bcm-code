function [proj_along_RF, proj_along_JF] = ...
        proj_along_int_src_windowed(o, oi, or, save_path, F_params)
%% --------------------------------------------------------------------
%% Function: proj_along_int_src(o, oi, save_path, F_params)
%% Use:      Compute the L^2 inner product (uf, RF) where F is an
%%           interior souce and uf is a solution to the wave equation.
%% --------------------------------------------------------------------
%% Inputs:
%% o         - the bcm discretization structure
%% oi        - the bcm discretization structure used for the interior 
%%             source problem
%% save_path - where data is saved for the wave equation with boundary
%%             sources
%% F_params  - the parameters descrbing the interior source
%% --------------------------------------------------------------------
%% Outputs:
%% proj_along_RF - the inner products:
%%                     (RF,uf_ij)_{ L^2(M(Gamma,R) \times [0,T]) }
%%                 where f_ij denote the basis functions
%% proj_along_JF - the inner products:
%%                     (JF,uf_ij)_{ L^2(M(Gamma,R) \times [0,T]) }
%%                 where f_ij denote the basis functions
%% --------------------------------------------------------------------

    %% get the basic solution and the coordinates
    [Ys,Xs] = meshgrid(o.ys_sol, o.xs_sol);
    
    %% get the sampling times
    Ts  = o.ts_rec(1 : o.mt_rec);
    
    % for performing the integrations
    dx  = o.sol_res ;
    dy  = o.sol_res ;
    dt  = o.dt      ;    
    
    %% tolerance to consider a function zero for windowing
    err_tol = min([dt^2, dx^2, dy^2, o.epsilon]);
    
    %% versions of F needed to figure out max
    F_full   = interior_source(F_params, Ts, Xs, Ys);
    
    %% find the index where the max occurs
    [MM, II]    = max(F_full(:));    
    [i_tM, i_xM, i_yM] = ind2sub(size(F_full), II);
    
    %% find the indices we want to keep
    Fys_atMaxT = F_full(i_tM, i_xM, :) / MM;
    Fxs_atMaxT = F_full(i_tM, :, i_yM) / MM;
    xinds = find(Fxs_atMaxT >= err_tol);
    yinds = find(Fys_atMaxT >= err_tol);
    
    %% get the windowed coordinates
    XsW = Xs(xinds, yinds);
    YsW = Ys(xinds, yinds);

    %% print out the window range
    yhi = max(max(YsW));
    ylo = min(min(YsW));
    xhi = max(max(XsW));
    xlo = min(min(XsW));
    fprintf(sprintf('\tWindow range = [%f,%f] x [%f,%f]\n', xlo,xhi,ylo,yhi));

    %% window F
    F   =   F_full(:, xinds, yinds);
    
    %% Weighted volume element for computing interior volume integrals
    [dV_g_mu,XsW2,YsW2] = compute_weighted_volume_element(o, save_path, save_path, ...
                                              XsW, YsW);    
    dV_g_mu = reshape(dV_g_mu, size(XsW));

    %% Apply the weighted volume element to F so we can compute
    %% integrals. Note that the weighting does not depend on time.
    for i = 1:o.mt_rec
        F(i, :, :) = dV_g_mu .* squeeze(F(i,:,:));
    end
    
    %% Compute the time reversal of F, RF
    RF  =             flipdim(F, 1);
    
    %% mid point for the half interval
    mt_rec_T_2 = ceil(o.mt_rec/2);
    %% integrals from t to T/2
    tmp1 =  dt * flipdim(cumsum(flipdim(F(1 : mt_rec_T_2, :, :), 1)), 1);
    %% integrals from T/2 to T-t
    tmp2 =  dt * flipdim(cumsum(     F(mt_rec_T_2 : end, :, :)), 1) ;
    %% JF(t) = 0.5 \int_t^{T/2 - t} F(s) ds 
    JF   = 0.5 * (tmp1 + tmp2);       
    
    %% flags, use exact data for debugging, use src sampling for 
    use_exact_data = false;    
    use_src_sampling = true || not(use_exact_data);    

    %% modify parameter to reflect sampling
    if use_src_sampling
        JF = JF(1 : o.ratio_t_separation_dt : end, :, :);
        RF = RF(1 : o.ratio_t_separation_dt : end, :, :);
        dt = o.ratio_t_separation_dt * dt;
        sample_type = 'src';
    else
        sample_type = 'rec';        
    end
    
    %% alocate space to store the inner-products
    proj_along_RF = zeros( o.nt_src, o.nx_src);
    proj_along_JF = zeros(oi.nt_src, o.nx_src);    

    %% compute the inner products
    for i = 1:o.nx_src
        if use_exact_data
            uf_i_1 = get_sol(o, [save_path '/sol'], i);
            if use_src_sampling
                uf_i_1 = uf_i_1(1 : o.ratio_t_separation_dt : end, :, :);
            end
        else
            uf_i_1 = get_L(o, or, save_path, i);
        end    
        tmp = size(uf_i_1);
        nt_u = tmp(1);
        mt_u = ceil(nt_u / 2);
        
        for j = 1:o.nt_src            
            uf_i_j = window_wavefield(o, uf_i_1, j, sample_type, xinds, yinds);
            uf_i_j_half = uf_i_j(1:mt_u,:,:);
            
            %% Apply the spacetime mask the entries            
            proj_along_RF(j,i) = uf_i_j(:)' * RF(:);

            if j <= oi.nt_src
                %% Compute the inner product
                proj_along_JF(j,i) = uf_i_j_half(:)' * JF(:);
            end
        end
    end

    %% finish the integration!
    proj_along_RF = dx * dy * dt * proj_along_RF;
    proj_along_JF = dx * dy * dt * proj_along_JF;
    
end
