"""
  Modification of Douglas Arnold's code.
  PAK: M:04/D:22/Y:14
  LSO: M:04/D:24/Y:14
  LSO: M:04/D:29/Y:14

  Domain: \Omega = (-X,X) \times (0,Y) 

  Newmark solver for the wave equation with Neumann boundary conditions:

    u_tt = Lap u            in          \Omega \times (0,2T)
    D_n u = f               on \partial \Omega \times (0,2T)
    u = 0 for t negative enough  

  Source f is a gaussian pulse centered at xs_src[i] in time and
  centered on ts_src[i] in space. Traces recorded (in space) on the set
  \Gamma = (-L,L) \times \{0\}.
"""

import time

from dolfin import *

from consolidate_probes import *

#the following is used to speed up pointwise evals
from numpy import *
from fenicstools.Probe import * #used to handle parallel file IO
import mpi4py

# if we want to export data to a mat file
import scipy.io as sio

comm = mpi4py.MPI.COMM_WORLD
rank = comm.Get_rank()
	
# essentially a wrapper for print. only print on
# process zero.
def prettier_print(s):
	if rank == 0:
		print(s)

# File i/o (no mathematics here)
import sys, os

def redirect_stdout():
    prettier_print("Redirecting stdout")
    sys.stdout.flush() # <--- important when redirecting to files

    # Duplicate stdout (file descriptor 1)
    # to a different file descriptor number
    newstdout = os.dup(1)

    # /dev/null is used just to discard what is being printed
    devnull = os.open('/dev/null', os.O_WRONLY)

    # Duplicate the file descriptor for /dev/null
    # and overwrite the value for stdout (file descriptor 1)
    os.dup2(devnull, 1)

    # Close devnull after duplication (no longer needed)
    os.close(devnull)

    # Use the original stdout to still be able
    # to print to stdout within python
    sys.stdout = os.fdopen(newstdout, 'w')

# to avoid IO repetition in parallel
# redirect_stdout()

# conditionally make directory
def conditionally_mkdir(dir_path):
	if rank == 0:
		if not os.path.exists(dir_path):
			os.makedirs(dir_path)

# sometimes I use the '-B' flag, but it throws off my the
# length of argv below. just get rid of it
if '-B' in sys.argv:
	sys.argv.pop(sys.argv.index('-B'))
# get the data path
data_path = sys.argv[1]
sys.path.append(data_path)

from femparams import *  

#print "Loaded femparams with save_path %s" % save_path
prettier_print("Loaded femparams with save_path " + data_path)

# what mode do we want to run the solver in?
coeff_mode       = (sys.argv[2] == 'coeff_mode')
trace_probe_mode = (sys.argv[2] == 'trace_probe_mode' or sys.argv[2] == 'all_probe_mode')
sol_probe_mode   = (sys.argv[2] == 'sol_probe_mode' or sys.argv[2] == 'all_probe_mode')
sol_mat_mode     = sys.argv[2] == 'sol_mat_mode'

if coeff_mode:
	prettier_print('RUNNING IN COEFF MODE')
	coeff_path = sys.argv[3]
	run_time   = bigT       # only need up to time T
	nt_ts      = int(ceil(nt / 2) + 1)	

	# where will we record plots?
	output_name = coeff_path.split('/')[-1].split('.')[0]
	output_path = coeff_path.split(output_name)[0]		
	the_file = File('{0}/{1}.pvd'.format(output_path, output_name))

if trace_probe_mode:
	prettier_print('TRACE MODE ON')
	run_time   = 2.0 * bigT # we need the full time
	nt_ts      = nt
	trace_ps = array([[x,0] for x in xs_rec])

if sol_probe_mode: 
	prettier_print('SOL MODE ON')
	sol_output_root   = sys.argv[3]
	if not trace_probe_mode:
		run_time          = bigT
		nt_ts             = int(ceil(nt / 2) + 1)
	sol_ps = array([[xx, yy] for yy in ys_sol for xx in xs_sol])

if sol_mat_mode:
	prettier_print('RUNNING IN SOL MAT MODE')
	run_time   = bigT       # only need up to time T
	nt_ts      = int(ceil(nt / 2) + 1)	

	# what time steps to record in the matfile?
	ts_src_sep = ts_src[1] - ts_src[0]
	nt_mat     = ceil(bigT / ts_src_sep)
	ts_mat     = linspace(0.0, bigT, nt_mat)
	i_mat      = 0


plot_frames = False
plotfreq = 10
def process_frame(j,t,u):
	if t > -0.1*dt: 
		prettier_print("Time = {0} (out of {1}), step {2}/{3} done".format(
				t, run_time, j, nt_ts - 1))
		if trace_probe_mode:
			trace_probes(u)
		if sol_probe_mode and t <= bigT + .1*dt:
			sol_probes(u)

	if (j % plotfreq) == 0 and plot_frames:
		plot(u, elevate=0.0, key='u')
	
	if coeff_mode and  abs(t - bigT) < .1 * dt:
		the_file << u

	if sol_mat_mode:
		global i_mat
		print '{0} {1}'.format(j, i_mat)
		if abs(t - ts_mat[i_mat]) < .1 * dt:
			zz = Vector()
			u.vector().gather(zz, array(range(V1.dim()), "intc"))
			if rank == 0:
				Zs_i = zz.array()
				Zs[i_mat,:] = Zs_i
				i_mat = i_mat + 1

if trace_probe_mode or sol_probe_mode or sol_mat_mode:
	# Source 
	code = '''
      #include <cmath>
      #include <dolfin/function/Function.h>
    
      namespace dolfin{
    
      class Gaussian : public Expression {  
    
        public:
          double t;        // time    
          double ax;       // spatial gaussian factor
          double at;       // temporal gaussian factor
          double xc;       // center of Gaussian in space
          double tc;       // center of Gaussian in time

          Gaussian() : Expression() {}

          void eval(Array<double>& values, const Array<double>& x) const {
           if(x[1] < .000001){   
             values[0] = exp(-ax*(x[0]-xc)*(x[0]-xc)) * exp(-at*(t-tc)*(t-tc));
           }
           else{
            values[0] = 0.;
           }
         }
       };
    }
    '''
	# set the source with some default values
	f = Expression(code, 
				   ax = ax,
				   at = at,
				   xc = xs_src[0],
				   tc = ts_src[0])

elif coeff_mode:
	# Source 
	code = '''
      #include <string>
      #include <fstream>
      #include <iostream>
      #include <cmath>
      #include <dolfin/function/Function.h>
    
      namespace dolfin{
    
      class Gaussian : public Expression {  
    
        public:
          double                  t;     // time    
          double                 ax;     // spatial gaussian factor
          double                 at;     // temporal gaussian factor

          std::vector<double>    xc;     // centers of Gaussian in space
          std::vector<double>    tc;     // centers of Gaussian in time

          std::vector<int>     i_xc;     // indices of Gaussians in space
          std::vector<int>     j_tc;     // indices of Gaussians in time
          std::vector<double> coeff;     // coefficients
          int               n_coeff;

          Gaussian() : Expression() {}

          void init_params(const std::vector< double >& xs_src, const std::vector< double >& ts_src, 
                           std::string coeff_path)
          {
            int i = 0;
            int i_xc, j_tc;
            double c_ij;
            std::ifstream ff;
            ff.open(coeff_path.c_str());

            while(ff >> i_xc >> j_tc >> c_ij){
              xc.push_back(xs_src[i_xc-1]);
              tc.push_back(ts_src[j_tc-1]);
              coeff.push_back(c_ij);
              i++;
            }
            n_coeff = coeff.size();
          }

          void show_params(){
            int k;
            for(k = 0; k < n_coeff; k++){
              std::cout << xc[k] << " " << tc[k] << " " << coeff[k] << std::endl;
            }
          }

          void eval(Array<double>& values, const Array<double>& x) const {
           int k;
           double vv = 0.0;
           if(x[1] < .000001){
             for(k = 0; k < n_coeff; k++){
               vv += coeff[k] * exp(-ax*(x[0]-xc[k])*(x[0]-xc[k])) * exp(-at*(t-tc[k])*(t-tc[k]));
             }
             values[0] = vv;
           }
           else{
            values[0] = 0.;
           }
         }
       };
    }
    '''
	# set the source with some default values
	print coeff_path
	f = Expression(code, 
				   ax = ax,
				   at = at)
				   

	f.init_params(array(xs_src, dtype=double), 
				  array(ts_src, dtype=double),
				  coeff_path)

#
# Newmark method over FEniCS 
#
# Initialize the mesh:
dolfin_version = int(dolfin.__version__.split('.')[1])
if dolfin_version < 6: 
	mesh = RectangleMesh(X0,0., X1,Y, nx,ny, "left/right")
else:
	mesh = RectangleMesh(Point(X0,0.), Point(X1,Y), nx,ny, "left/right")

# Initialize function space and basis functions on mesh:
V = FunctionSpace(mesh, 'CG', deg)

# Define Functions on V:
u = TrialFunction(V)     # function form for solution
phi = TestFunction(V)    # function form for test function

# Define the sound-speed from 'c_code.' Included from wave_speed.py
c = Expression(c_code)

#   print "Assembling mass matrix"
prettier_print("Assembling mass matrix")
M = assemble(c**(-2) * u * phi *dx)

#   print "Assembling stiffness matrix"
prettier_print("Assembling stiffness matrix")
A = assemble(inner(grad(u),grad(phi))*dx)

# Matrices used in time-stepping
B =      M + dt**2*A/4.0
C =  2.0*M - dt**2*A/2.0
D = -1.0*M - dt**2*A/4.0

def run_solver():
	# Initially everything vanishes
	f0 = interpolate(Constant(0.0), V)
	fcurvec = assemble(c**(-2) * f0 * phi * ds)
	foldvec = fcurvec
	ucur = interpolate(Constant(0.0), V)
	uold = interpolate(Constant(0.0), V)
	unew = Function(V)
	
	# If we do NOT provide an initial time margin,
	# then we immediately start recording, so we need
	# to handle this possibility
	if nt_margin == 0:
		# save t = 0
		process_frame(0,0,uold)
		# save t = dt
		process_frame(1,dt,ucur)
	
	prettier_print("Timestepping")
	for j in range(2-nt_margin, nt_ts):
		f.t = j*dt
		fnewvec = assemble(c**(-2) * f * phi * ds)
		L = C*ucur.vector() + D*uold.vector() + dt**2 * (foldvec + 2.* fcurvec + fnewvec)/4.
		
		solve(B, unew.vector(), L, "cg")
		
		uold.assign(ucur)
		ucur.assign(unew)
		foldvec = fcurvec
		fcurvec = fnewvec
		process_frame(j, f.t, ucur)


# For plotting purposes
if sol_mat_mode:
	if deg > 1:
		V1 = FunctionSpace(mesh, 'CG', 1)
	elif deg == 1:
		V1 = V

	## grab the coordinates from the mesh save them in variables Xs
	## and Ys ONLY ON PROCESSOR 0
	dd = mesh.geometry().dim()                                                
	dof_coordinates = V1.dofmap().tabulate_all_coordinates(mesh)
	n_coords = size(dof_coordinates) / dd
	dof_coordinates.resize((n_coords, dd))
	xx = dof_coordinates[:,0]
	yy = dof_coordinates[:,1]
	xx = comm.gather(xx, root=0)
	yy = comm.gather(yy, root=0)
	if rank == 0:
		Xs = []
		Ys = []
		for i in range(comm.Get_size()):
			Xs.extend(list(xx[i]))
			Ys.extend(list(yy[i]))
		Xs = array(Xs)
		Ys = array(Ys)
	
		Zs = zeros((nt_mat, n_coords))


# Run the relevant simulation:
if trace_probe_mode or sol_probe_mode or sol_mat_mode:
	if transl_invar_flag:
		# if the model is translation invariant
		# then we only need to compute trace data for one 
		# source location
		nx_src = len(xs_src)
		mx_src = int(ceil(nx_src/2))
		index_range = [mx_src]
	else:
		index_range = range(0, len(xs_src))

	# generate trace data
	for i_xc in index_range:

		# begin by assuming we need to run the solver, will update below.
		run_solver_flag = True
		
		if trace_probe_mode:
			# probes to do the function evaulation, they require V
			trace_probes = Probes(trace_ps.flatten(),V)
			# set up directories to hold traces
			trace_root = '{0}/trace'.format(save_path)
			current_trace = 'trace_{0}'.format(i_xc + 1)
			trace_path = '{0}/{1}'.format(trace_root, current_trace)
			conditionally_mkdir(trace_root)
			conditionally_mkdir(trace_path)
			# check to see if there is a consolidated trace probe file for the
			# current probe
			run_solver_flag = run_solver_flag or not(os.path.exists('{0}/{1}.txt'.format(trace_root, current_trace)))

		if sol_probe_mode:
			# probes to do the function evaulation, they require V
			sol_probes = Probes(sol_ps.flatten(),V)
			# set up directories to hold sols
			sol_root = '{0}/sol'.format(save_path)
			current_sol = 'sol_{0}'.format(i_xc + 1)
			sol_path = '{0}/{1}'.format(sol_root, current_sol)	
			conditionally_mkdir(sol_root)
			conditionally_mkdir(sol_path)

			# check to see if there is a consolidated sol probe file for the
			# current probe
			run_solver_flag = run_solver_flag or not(os.path.exists('{0}/{1}.txt'.format(sol_root, current_sol)))

		if sol_mat_mode:
			# set up directories to hold matfiles
			sol_root = '{0}/sol'.format(save_path)
			current_sol = 'sol_{0}'.format(i_xc + 1)
			sol_path = '{0}/{1}'.format(sol_root, current_sol)	
			sol_mat_path  = '{0}/{1}.mat'.format(sol_path, current_sol)
			conditionally_mkdir(sol_root)
			conditionally_mkdir(sol_path)
				
			# check to see if there is a consolidated sol probe file for the
			# current probe
			run_solver_flag = run_solver_flag or not(os.path.exists(sol_mat_path))


		if run_solver_flag:
			if trace_probe_mode:
				prettier_print('Computing the trace with source xs[{0}] = {1}'.format(i_xc+1, xs_src[i_xc]))
			if sol_probe_mode:
				prettier_print('Computing the soln  with source xs[{0}] = {1}'.format(i_xc+1, xs_src[i_xc]))

			# set the source spatial center
			f.xc = xs_src[i_xc]
			# run the solver
			run_solver()

			if trace_probe_mode:
				# dump the traces to file and then consolidate them
				trace_probes.dump('{0}/trace'.format(trace_path))
				mpi4py.MPI.COMM_WORLD.Barrier()
				if rank == 0:
					consolidateProbes(trace_root, current_trace, 'trace')

			if sol_probe_mode:
				# dump the sols to file and then consolidate them
				sol_probes.dump('{0}/sol'.format(sol_path))
				mpi4py.MPI.COMM_WORLD.Barrier()
				if rank == 0:
					consolidateProbes(sol_root, current_sol, 'sol')

			if sol_mat_mode:
				# save Xs Ys coordinates:
				if rank == 0:
					sio.savemat(sol_mat_path, {'Xs' : Xs, 'Ys' : Ys, 'Zs' : Zs})

				
elif coeff_mode:
	run_solver()
