function coeff_path = save_coeffs(o, save_path, coeffs, output_path, filename)   
%% --------------------------------------------------------------------
%% Function: save_coeffs(o, save_path, coeffs, output_path, filename)
%% Use:      save coefficients for use in the wave solver. Note, the
%%           wave solver does NOT use normalized basis functions,
%%           so the coefficients saved here are pre-multiplied by the
%%           basis normalization 
%% --------------------------------------------------------------------
%% Inputs:
%% o           - the bcm discretization structure
%% save_path   - where everything is saved
%% coeffs      - the coefficient vector to save
%% output_path - directory to output coefficients to
%% file_name   - name of the coefficients file
%% --------------------------------------------------------------------
%% Outputs:
%% coeff_path  - the file path for the coefficient file
%% --------------------------------------------------------------------

    nt_src = o.nt_src;
    nx_src = o.nx_src;

    coeff_path = [output_path '/' filename '.txt'];
    fid = fopen(coeff_path, 'w');

    c = reshape(coeffs, nt_src, nx_src);

    for i=1:nx_src
        for j =1:nt_src

            %% the wave solver does not use normalized basis functions, so we
            %% must normalize here!
            c(j,i) = o.normalization_Lambda(i) * c(j,i);
            
            if abs(c(j,i)) > o.epsilon
                fprintf(fid, sprintf('%d %d %f\n', i, j, c(j,i)));
            end
        end
    end
    
    fclose(fid);
end