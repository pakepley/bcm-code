function o = basis_x_normalizations(o, save_path)
%% --------------------------------------------------------------------
%% Function: basis_x_normalizations
%% Use:      compute the normalizations of the basis functions sampled
%%           on xs_rec
%% --------------------------------------------------------------------
%% Inputs:
%% o         - the bcm discretization structure
%% save_path - where everything is saved
%% --------------------------------------------------------------------
%% Outputs:
%% o      - the bcm discretization structure updated to include
%%          the spatial and Lambda normalizations
%% --------------------------------------------------------------------

    % compute and load the values of c on xs_rec 
    system(['python ./' o.domain_type '/compute_c_on_xs_rec.py ' save_path]);
    load([save_path '/c_at_xs_rec.mat']);
        
    %% set a dummy normalization
    o.normalization_basis_x = ones(o.nx_src,1);

    %% compute the correct normalization
    normalization_basis_x = zeros(o.nx_src,1);
    dx  = o.dx;
    for i = 1:o.nx_src
        yy = fun_basis_x(o, o.xs_rec, i);
        
        %% Recall that dS_{g,mu} = c^{-2}ds, so:
        %%    
        %%  \|\phi_x\|_{L^2(Gamma,dS_{g,mu} = 
        %%            \sqrt(\int_\Gamma  \phi_x^2 c^{-2} ds )
        %%
        normalization_basis_x(i) = 1.0 / sqrt(sum(yy.^2 ./ c_at_xs_rec.^2) * o.dx);
    end
   
    o.normalization_basis_x = normalization_basis_x;
    o.normalization_Lambda = o.normalization_basis_t * o.normalization_basis_x;

    o.c_at_xs_rec = c_at_xs_rec;
end