function out = fun_basis_x(o, x, j)
%% --------------------------------------------------------------------
%% Function: fun_basis_x
%% Use:      compute the jth spatial basis function
%% --------------------------------------------------------------------
%% Inputs:
%% o    - the bcm discretization structure
%% x    - the x coordinates where we evaluate the basis function
%% j    - the index of the basis function that we are evaluting
%% --------------------------------------------------------------------
%% Outputs:
%% out  - the values of the jth spatial basis function evaluated on
%%        the x-coordinates
%% --------------------------------------------------------------------

    d = 0.5*o.wx;
    a = o.xs_src(j) - d;
    b = o.xs_src(j) + d;
    c = o.xs_src(j);

    out = o.normalization_basis_x(j) * (x > a & x < b) .* exp(- o.ax * (x - c).^2);

end

