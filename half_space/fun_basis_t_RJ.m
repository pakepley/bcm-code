function out = fun_basis_t_RJ(o, t, j)
%% --------------------------------------------------------------------
%% Function: fun_basis_t_RJ
%% Use:      compute RJ(phi_j) for the jth temporal basis function,
%%           phi_j
%% --------------------------------------------------------------------
%% Inputs:
%% o    - the bcm discretization structure
%% t    - the t coordinates where we evaluate the basis function
%% j    - the index of the basis function that we are evaluting
%% --------------------------------------------------------------------
%% Outputs:
%% out  - the values of the RJ(phi_j) for the jth temporal basis 
%%        function, evaluated on the t-coordinates
%% --------------------------------------------------------------------

    a = o.ts_src(j);
    b = o.ts_src(j)+o.wt;
    T = o.T;

    % We use normcdf() to compute Gaussian integrals. But we do not use
    % the probablistic normalization present in normcdf(). So we need to 
    % multiply by sigma * sqrt(2 * pi) to get rid of that.

    mu = o.ts_src(j);
    sigma = sqrt(1.0/(2 * o.at));

    out = o.normalization_basis_t * 0.5 * (sigma * sqrt(2 * pi)) * (...
		   normcdf(T+t,mu,sigma) - normcdf(T - t,mu,sigma));
end

