import sys, os
import numpy as np
import scipy.io as sio

# get the simulation save_path, where
# the file wave_speed.py lives
data_path = sys.argv[1]
sys.path.append(data_path)

# grab the wave-speed from file
from wave_speed import *

# where have saved the locations we want to evaluate
# the volume form at:
volume_weight_matfile = sys.argv[2]
vol_weight_data = sio.loadmat(volume_weight_matfile)

# grab the coordinates
Xs = vol_weight_data['Xs'].flatten(1)
Ys = vol_weight_data['Ys'].flatten(1)
dV_g_mu = np.zeros(np.shape(Xs))

# evaluate the weighted volume element. for now
for i in range(len(Xs)):
	dV_g_mu[i] = c([Xs[i], Ys[i]])**(-2)

vol_weight_data = sio.loadmat(volume_weight_matfile)

sio.savemat(volume_weight_matfile, 
			{'Xs':Xs, 'Ys':Ys, 'dV_g_mu':dV_g_mu}, 
			oned_as='row')
