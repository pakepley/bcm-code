function out = fun_basis_t(o, t, j)
%% --------------------------------------------------------------------
%% Function: fun_basis_t
%% Use:      compute the jth temporal basis function
%% --------------------------------------------------------------------
%% Inputs:
%% o    - the bcm discretization structure
%% t    - the t coordinates where we evaluate the basis function
%% j    - the index of the basis function that we are evaluting
%% --------------------------------------------------------------------
%% Outputs:
%% out  - the values of the jth temporal basis function evaluated on
%%        the t-coordinates
%% --------------------------------------------------------------------

    d = 0.5*o.wt;
    a = o.ts_src(j) - d;
    b = o.ts_src(j) + d;
    c = o.ts_src(j);

    out = o.normalization_basis_t * (t > a & t < b) .* exp(- o.at * (t - c).^2);
end

