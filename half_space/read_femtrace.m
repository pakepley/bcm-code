function Lambda = read_femtrace(o, save_path)
%% --------------------------------------------------------------------
%% Function: read_femtrace
%% Use:      read the trace and generate the approximation to Lambda
%% --------------------------------------------------------------------
%% Inputs:
%% o          - the bcm discretization structure
%% save_path  - where the data was saved
%% --------------------------------------------------------------------
%% Outputs:
%% Lambda     - the approximation to the N-D map
%% --------------------------------------------------------------------

mx_src = o.mx_src;
mx_rec = ceil(o.nx_rec / 2);
nx_src = o.nx_src;
nx_rec = o.nx_rec;
nt_rec = o.nt_rec;

% Reserve space for the Lambdas
Lambda = zeros(nt_rec, nx_rec, nx_src);

if o.transl_invar_flag
    %% ----------------------------------------------------------------
    %% Translation invariant case:
    %% ----------------------------------------------------------------
    %% read the trace associated with xs_src[mx_src]
    %% use translation invariance to popluate Lambda
    %%
    trace_mx_src = dlmread(sprintf([save_path '/trace/trace_%d.txt'], mx_src));        

    %% Note: in the bcm case trace_mx_src will always be of length nt_rec,
    %%       but in the moving sources case, trace_mx_src will have
    %%       length 2 * nt_rec. This is because trace_mx_src is computed
    %%       with a discretization o that records 2 * T units of time,
    %%       whereas the moving sources case uses T units of time.
    %%       So we must truncate accordingly.
    trace_mx_src = trace_mx_src(1 : nt_rec, :);
    
    for j=1:nx_src
        shift = o.ratio_x_separation_dx * abs(j - mx_src);
        if j < mx_src
            % j <= mx_src, zero pad on the right
            Lambda(:, 1 : nx_rec - shift + 1, j) = ...
                trace_mx_src(:, shift : nx_rec);
        elseif j == mx_src
            Lambda(:, :, j) = trace_mx_src;
        elseif j > mx_src
            % j > mx_src, zero pad on the left
            % j <= mx_src, zero pad on the right
            Lambda(:, shift : nx_rec, j) = ...
                trace_mx_src(:, 1 : nx_rec - shift + 1);
        end
    end
else
    %% ----------------------------------------------------------------
    %% Non-translation invariant case:
    %% ----------------------------------------------------------------
    %% we must read all of the traces for each xs_src[j] to 
    %% popluate lambda
    %%
    for j=1:nx_src
        trace_j = dlmread(sprintf([save_path '/trace/trace_%d.txt'], j));
        Lambda(:, :, j)   =   trace_j;
    end
end
