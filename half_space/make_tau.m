function tau = make_tau(o,s,y,h,z,r)
%% --------------------------------------------------------------------
%% Function : make_tau(o,s,y,h,z,r)
%% Use      : generate a tau function to use for source restriction 
%%            masking when solving the BC problem
%% --------------------------------------------------------------------
%% Inputs:
%% o    - the bcm discretization structure
%% s    - the height of the flat portion 
%% --------------------------------------------------------------------
%% Optional Inputs:
%% y,z  - wave-cap base points. When both are used, y refers to
%%        to the target cap base-point and z refers to the variable
%%        cap base-point        
%% h,r  - heights for up to two wave-caps. When both are used, s+h is 
%%        the target cap radius and s+r is the variable cap radius
%% --------------------------------------------------------------------
%% Outputs:
%% tau  - the supporting function tau sampled on o.xs_src
%% --------------------------------------------------------------------
    
    
    switch nargin
      case 2      
        % only s is  specified --> flat portion only
        tau = s * ones(size(o.xs_src));

      case 3      
        % only y,s are  specified --> disk only
        tau  = max(s - abs(o.xs_src - y), 0);
        
      case 4      
        % s,y,h      specified --> only one cap
        tau_flat        = s * ones(size(o.xs_src));
        tau_disk_target = (s + h) - abs(o.xs_src - y);
        tau = max(tau_flat, tau_disk_target);
      
      case 6      
        % s,y,h,z,r  specified --> two caps
        tau_flat          = s * ones(size(o.xs_src));
        tau_disk_target   = (s + h) - abs(o.xs_src - y);
        tau_disk_variable = (s + r) - abs(o.xs_src - z);
        tau               = max(tau_flat, ...
                                max(tau_disk_target, ...
                                    tau_disk_variable));        
    end    

end