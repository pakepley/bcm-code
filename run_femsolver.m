function run_femsolver(o, save_path, solver_mode, output_path)

data_type = o.data_type;

if strcmp(data_type, 'ND')
    solver_filename = 'wave_neumann.py';
elseif strcmp(data_type, 'ND_INT')
    solver_filename = 'wave_interior.py';
else
    error(['Inappropriate data_type in discretization. Must be either ND\n' ...
           '(for boundary problem) or ND_INT (for interior problem)']);
end

% what mode should we run the solver in?
if not(exist('solver_mode', 'var'))
    if strcmp(data_type, 'ND')
        solver_mode = 'trace_probe_mode';
    elseif strcmp(data_type, 'ND_INT')
        solver_mode = '';
    end            
end

% where should we put secondary output?
if not(exist('output_path','var'))
    if strcmp(solver_mode, 'sol_probe_mode') || strcmp(solver_mode, ...
                                                   'all_probe_mode')
        output_path = save_path;
    else
        output_path = '';
    end
end

solver_command = ['python ./' o.domain_type '/' solver_filename ' ' ... 
                  save_path ' -B ' solver_mode ' ' output_path ';'];

if o.n_processors >= 2
    solver_command = [sprintf('mpirun -n %d ', o.n_processors) ...
                      solver_command];
end

fprintf('%s\n', solver_command);
status = system(solver_command);

    
% set LD_LIBRARY_PATH as a workaround for 
% http://fenicsproject.org/qa/156/error-calling-the-fenics-from-matlab
% With the option -B Python won’t try to write .pyc or .pyo files
if status ~= 0
    warning(['Fem solver exited with non zero status\n' ...
             'Sometimes this will happen due to graphics on my machine,\n' ...
             'checking to see if data was recorded...\n']);
    
    fprintf('\n');
    fprintf('Try running the following on the command line:\n');
    fprintf(solver_command);
    fprintf('\n\n');
end

end