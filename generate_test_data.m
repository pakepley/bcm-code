function [rhs_integral, f_coeffs] = generate_test_data(o, t_c, p_c, sigma_h)
%% --------------------------------------------------------------------
%% Function : generate_test_data(o)
%% Use      : generate some test data for the wavefield reconstruction 
%%            (i.e. redatuming) problem. In particular, we generate
%%            coefficients of a Gaussian pulse projected into the basis
%%            with central time t_c, location p_c (in appropriate 
%%            coordinates) and scaling parameter sigma_h.
%% --------------------------------------------------------------------
%% Inputs: 
%% o       - the bcm discretization structure    
%% t_c     - central time for the source
%% p_c     - central location (in coordinates) for the source
%% sigma_h - scaling paramter (this is almost the standard deviation, 
%%           but not quite)
%% --------------------------------------------------------------------
%% Outputs:
%% rhs_integral - the integrals of Kf against the basis functions
%% --------------------------------------------------------------------
        
    mt_rec = o.mt_rec;    

    if strcmp(o.domain_type, 'disk')
        [Thetas,Ts] = meshgrid(o.thetas_rec, o.ts_rec(1:mt_rec));
        theta_c = p_c;        
        f = exp(-((Thetas - theta_c).^2 + (Ts - t_c).^2)/sigma_h^2);
    
    elseif strcmp(o.domain_type, 'half_space')
        [Xs,Ts] = meshgrid(o.xs_rec, o.ts_rec(1:mt_rec));
        x_c = p_c;
        f = exp(-((Xs - x_c).^2 + (Ts - t_c).^2)/sigma_h^2);
    end

    f_integral   =             proj_basis(o,f) ;
    f_coeffs     =  o.coeffs_invG * f_integral ;
    rhs_coeffs   = o.coeffs_invGK *   f_coeffs ;
    rhs_integral =     o.coeffs_G * rhs_coeffs ;
    
end
