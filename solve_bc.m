function [f, mask] = solve_bc(o, os, rhs_integral, tau)
%% --------------------------------------------------------------------
%% Function: solve_bc
%% Use:      solve the boundary control problem using the function tau
%%           as a support constraint
%% --------------------------------------------------------------------
%% Inputs:
%% o       - the bcm discretization structure
%% os      - inversion parameters 
%% tau     - the supporting function tau sampled on o.xs_src
%% --------------------------------------------------------------------
%% Outputs:
%% f       - the source which approximately solves: 
%%                    K f = rhs_integral
%% mask    - the mask used to enforce the support constraint arising
%%           from tau
%% --------------------------------------------------------------------
    
    % Grab the inversion parameters from os
    Nouter    = os.Nouter;
    Nrestart  = os.Nrestart;
    alpha     = os.alpha;
    tolerance = os.tolerance;
    
    % Logical matrix, used to select entries
    mask = make_mask(o, tau);

    % How many elements?
    if strcmp(o.domain_type, 'disk')
        N = o.nt_src * o.ntheta_src;
    elseif strcmp(o.domain_type, 'half_space')
        N = o.nt_src * o.nx_src;
    end

    function out = unmask(z)
        out = zeros(N, 1);
        out(mask) = z;
    end    
    
    NNZZ = nnz(mask);
    
    %% invGK = o.invG * o.K
    %KK_alpha =  o.coeffs_invGK + alpha * eye(size(o.coeffs_invGK));
    % P_tau    =              logical( (1.0 * mask(:)) * (1.0 * mask(:))');
    % KK_am    = reshape( KK_alpha(P_tau), NNZZ, NNZZ);
    %KK_am = KK_alpha(mask, mask);
    KK_alpha = o.coeffs_invGK(mask, mask); % Mask
    KK_alpha = KK_alpha + alpha * eye(NNZZ);
            
    % coefficients of rhs in basis
    rhs_coeff = o.coeffs_invG  *  rhs_integral ;
    
    % inputs to A must be masked, also outputs (ie f) will be masked, so no loss here 
    rhs_coeff = rhs_coeff(mask);                                         
    
    % compute the coefficients of z.
    z = gmres(KK_alpha, rhs_coeff, Nrestart, tolerance, Nouter);
    
    %z  = KK_am \ rhs_coeff;
    
    f = unmask(z);
end
